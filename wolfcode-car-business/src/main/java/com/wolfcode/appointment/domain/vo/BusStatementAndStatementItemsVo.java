package com.wolfcode.appointment.domain.vo;

import com.wolfcode.appointment.domain.BusStatementItem;
import lombok.Data;

import java.util.List;

@Data
public class BusStatementAndStatementItemsVo {
    /**
     * 结算单数据
     */
    private BusStatementWithItemVo busStatementWithItemVo;
    /**
     * 结算单明细
     */
    private List<BusStatementItem> busStatementItems;
}
