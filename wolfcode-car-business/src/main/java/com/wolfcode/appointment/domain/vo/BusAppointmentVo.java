package com.wolfcode.appointment.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wolfcode.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

@Data
public class BusAppointmentVo {
    private Long id;
    /** 客户姓名 */
    private String customerName;
    /** 客户联系方式 */
    private String customerPhone;
    /** 预约时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date appointmentTime;
    /** 车牌号码 */
    private String licensePlate;
    /** 汽车类型 */
    private String carSeries;
    /** 服务类型【维修0/保养1】 */
    private Integer serviceType;
    /** 备注信息 */
    private String info;
}
