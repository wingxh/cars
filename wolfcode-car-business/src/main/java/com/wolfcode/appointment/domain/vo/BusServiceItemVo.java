package com.wolfcode.appointment.domain.vo;

import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 服务项对象 bus_service_item
 * 
 * @author wolfcode
 * @date 2022-11-22
 */
@Data
public class BusServiceItemVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;
    /** 服务项名称 */
    private String name;
    /** 服务项原价 */
    private BigDecimal originalPrice;
    /** 服务项折扣价 */
    private BigDecimal discountPrice;
    /** 是否套餐【是/否】 */
    private Integer carPackage;
    /** 备注信息 */
    private String info;
    /** 服务分类【维修/保养/其他】 */
    private Integer serviceCatalog;

}
