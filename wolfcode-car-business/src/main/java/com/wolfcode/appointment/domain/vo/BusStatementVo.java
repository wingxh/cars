package com.wolfcode.appointment.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 结算单对象 bus_statement
 * 
 * @author wolfcode
 * @date 2022-11-23
 */
@Data
public class BusStatementVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    /** $column.columnComment */
    private Long id;
    /** 客户姓名 */
    private String customerName;
    /** 客户联系方式 */
    private String customerPhone;
    /** 实际到店时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date actualArrivalTime;
    /** 车牌号码 */
    private String licensePlate;
    /** 汽车类型 */
    private String carSeries;
    /** 服务类型【维修/保养】 */
    private Integer serviceType;
    /** 备注信息 */
    private String info;
}
