package com.wolfcode.appointment.domain.info;

import com.wolfcode.appointment.domain.BusServiceItem;
import com.wolfcode.common.core.domain.entity.SysUser;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ServiceItemAuditInfo {
    /**
     * 要审核的服务项
     */
    private BusServiceItem serviceItem;
    /**
     * 店长
     */
    private List<SysUser> shopOwners;
    /**
     * 财务
     */
    private List<SysUser> finances;
    /**
     * 流程部署文件--服务套餐--业务判断标准
     */
    private BigDecimal discountPrice;
}
