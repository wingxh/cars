package com.wolfcode.appointment.domain;

import java.math.BigDecimal;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;

/**
 * 结算单明细对象 bus_statement_item
 * 
 * @author wolfcode
 * @date 2022-11-25
 */
@Data
public class BusStatementItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    /** $column.columnComment */
    private Long id;
    /** 结算单ID */
    @Excel(name = "结算单ID")
    private Long statementId;
    /** 服务项明细ID */
    @Excel(name = "服务项明细ID")
    private Long itemId;
    /** 服务项明细名称 */
    @Excel(name = "服务项明细名称")
    private String itemName;
    /** 服务项价格 */
    @Excel(name = "服务项价格")
    private BigDecimal itemPrice;
    /** 购买数量 */
    @Excel(name = "购买数量")
    private Long itemQuantity;
}
