package com.wolfcode.appointment.enums;

public enum  BusAppointmentEnum {
    //预约中0
    APPOINTING,
    //已到店1
    ARRIVAL,
    //用户取消2
    CANCEL,
    //超时取消3
    TIMEOUT_CANCEL,
    //已结算4
    STATEMENT,
    //已支付5
    PAYED

}
