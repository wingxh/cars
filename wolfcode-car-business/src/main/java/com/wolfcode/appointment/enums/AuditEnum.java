package com.wolfcode.appointment.enums;

/**
 * 审核状态
 */
public enum AuditEnum {
    // 初始化
    INIT,
    // 审核中
    AUDITING,
    // 审核通过
    PASS,
    // 审核拒绝
    REJECT,
    // 无需审核
    NO_AUDIT
}
