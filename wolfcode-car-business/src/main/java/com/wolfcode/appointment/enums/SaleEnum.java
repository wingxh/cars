package com.wolfcode.appointment.enums;

public enum SaleEnum {
    // 未下架 0
    NO,
    // 已上架 1
    YES
}
