package com.wolfcode.appointment.enums;

public enum  StatementEnum {
    /**
     * 消费中
     */
    COSTING,
    /**
     * 已支付
     */
    PAYED
}
