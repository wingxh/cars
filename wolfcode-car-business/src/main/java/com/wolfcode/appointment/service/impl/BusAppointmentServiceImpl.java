package com.wolfcode.appointment.service.impl;

import java.util.Date;
import java.util.List;

import com.wolfcode.appointment.domain.vo.BusAppointmentVo;
import com.wolfcode.appointment.enums.BusAppointmentEnum;
import com.wolfcode.common.utils.DateUtils;
import com.wolfcode.common.utils.PhoneUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wolfcode.appointment.mapper.BusAppointmentMapper;
import com.wolfcode.appointment.domain.BusAppointment;
import com.wolfcode.appointment.service.IBusAppointmentService;
import org.springframework.util.Assert;

/**
 * 养修信息预约Service业务层处理
 * 
 * @author wolfcode
 * @date 2022-11-20
 */
@Service
public class BusAppointmentServiceImpl implements IBusAppointmentService 
{
    @Autowired
    private BusAppointmentMapper busAppointmentMapper;

    /**
     * 查询养修信息预约
     * 
     * @param id 养修信息预约主键
     * @return 养修信息预约
     */
    @Override
    public BusAppointment selectBusAppointmentById(Long id)
    {
        return busAppointmentMapper.selectBusAppointmentById(id);
    }

    /**
     * 查询养修信息预约列表
     * 
     * @param busAppointment 养修信息预约
     * @return 养修信息预约
     */
    @Override
    public List<BusAppointment> selectBusAppointmentList(BusAppointment busAppointment)
    {
        return busAppointmentMapper.selectBusAppointmentList(busAppointment);
    }

    /**
     * 新增养修信息预约
     * 
     * @param
     * @return 结果
     */
    @Override
    public int insertBusAppointment(BusAppointmentVo busAppointmentVo)
    {
        // 数据校验
        Assert.notNull(busAppointmentVo,"非法参数");
        // 手机号校验
        Assert.state(PhoneUtil.isMobileNumber(
                busAppointmentVo.getCustomerPhone()),"手机格式不正确");
        // 预约时间不能今天之前
//        Date appointmentTime = busAppointmentVo.getAppointmentTime();
//        Date today=DateUtils.getNowDate();
//        // 2022-11-19        2022-11-20
//        appointmentTime.before(today);  // true
        Assert.state(!busAppointmentVo.getAppointmentTime()
                .before(DateUtils.getNowDate()),"预约时间不能今天之前");
        BusAppointment busAppointment=new BusAppointment();
        BeanUtils.copyProperties(busAppointmentVo,busAppointment);
        busAppointment.setCreateTime(DateUtils.getNowDate());
        return busAppointmentMapper.insertBusAppointment(busAppointment);
    }

    /**
     * 修改养修信息预约
     * 
     * @param
     * @return 结果
     */
    @Override
    public int updateBusAppointment(BusAppointmentVo busAppointmentVo)
    {
        Assert.notNull(busAppointmentVo,"非法参数");
        // 手机号校验
        Assert.state(PhoneUtil.isMobileNumber(
                busAppointmentVo.getCustomerPhone()),"手机格式不正确");
        // 预约时间不能今天之前
        Assert.state(!busAppointmentVo.getAppointmentTime()
                .before(DateUtils.getNowDate()),"预约时间不能今天之前");
        //只有预约中，才可以修改
        // 通过预约id，去数据库中查询，查询出状态
        // TODO 提高性能，可以先查询redis，查询不到再查数据库，作业
        BusAppointment appointment = busAppointmentMapper
                .selectBusAppointmentById(busAppointmentVo.getId());
        Integer status = appointment.getStatus();
        Assert.state(status== BusAppointmentEnum.APPOINTING.ordinal(),
                "只有预约中，才可以修改");
        BusAppointment busAppointment=new BusAppointment();
        BeanUtils.copyProperties(busAppointmentVo,busAppointment);
        return busAppointmentMapper.updateBusAppointment(busAppointment);
    }

    /**
     * 批量删除养修信息预约
     * 
     * @param ids 需要删除的养修信息预约主键
     * @return 结果
     */
    @Override
    public int deleteBusAppointmentByIds(Long[] ids)
    {
        return busAppointmentMapper.deleteBusAppointmentByIds(ids);
    }

    /**
     * 删除养修信息预约信息
     * 
     * @param id 养修信息预约主键
     * @return 结果
     */
    @Override
    public int deleteBusAppointmentById(Long id)
    {
        return busAppointmentMapper.deleteBusAppointmentById(id);
    }
    /**
     * 预约信息取消
     *
     * @param id 养修信息预约主键
     * @return 结果
     */
    @Override
    public int cancel(Long id) {
        Assert.notNull(id,"非法参数");
        //1 在什么状态下才能取消
        BusAppointment busAppointment = busAppointmentMapper.selectBusAppointmentById(id);
        Assert.state(busAppointment.getStatus()==
                BusAppointmentEnum.APPOINTING.ordinal(),"在预约中才可以取消");
        //2 修改状态2 用户取消
        BusAppointment appointment=new BusAppointment();
        appointment.setId(id);
        appointment.setStatus(BusAppointmentEnum.CANCEL.ordinal());
        // update bus_appointment set status=CANCEL where id=id
        // update bus_appointment set name='',xxx='', status=CANCEL where id=id
        return  busAppointmentMapper.updateBusAppointment(appointment);
    }

    /**
     * 到店
     * @param id 养修信息预约主键
     * @return
     */
    @Override
    public int arrive(Long id) {
        Assert.notNull(id,"非法参数");
        //1 在什么状态下才能到店
        BusAppointment busAppointment = busAppointmentMapper.selectBusAppointmentById(id);
        Assert.state(busAppointment.getStatus()==
                BusAppointmentEnum.APPOINTING.ordinal(),"在预约中才可以取消");
        //2 修改状态1 已到店 , 到店时间
        BusAppointment appointment=new BusAppointment();
        appointment.setId(id);
        appointment.setStatus(BusAppointmentEnum.ARRIVAL.ordinal());
        // 到店时间
        appointment.setActualArrivalTime(DateUtils.getNowDate());
        return busAppointmentMapper.updateBusAppointment(appointment);
    }
}
