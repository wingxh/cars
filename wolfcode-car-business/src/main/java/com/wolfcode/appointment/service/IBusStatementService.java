package com.wolfcode.appointment.service;

import java.util.List;
import com.wolfcode.appointment.domain.BusStatement;
import com.wolfcode.appointment.domain.vo.BusStatementVo;

/**
 * 结算单Service接口
 * 
 * @author wolfcode
 * @date 2022-11-23
 */
public interface IBusStatementService 
{
    /**
     * 查询结算单
     * 
     * @param id 结算单主键
     * @return 结算单
     */
    public BusStatement selectBusStatementById(Long id);

    /**
     * 查询结算单列表
     * 
     * @param busStatement 结算单
     * @return 结算单集合
     */
    public List<BusStatement> selectBusStatementList(BusStatement busStatement);

    /**
     * 新增结算单
     * 
     * @param
     * @return 结果
     */
    public int insertBusStatement(BusStatementVo busStatementVo);

    /**
     * 修改结算单
     * 
     * @param
     * @return 结果
     */
    public int updateBusStatement(BusStatementVo busStatementVo);

    /**
     * 批量删除结算单
     * 
     * @param ids 需要删除的结算单主键集合
     * @return 结果
     */
    public int deleteBusStatementByIds(Long[] ids);

    /**
     * 删除结算单信息
     * 
     * @param id 结算单主键
     * @return 结果
     */
    public int deleteBusStatementById(Long id);

    /**
     * 删除，逻辑删除
     * @return
     */
    public int removeById(Long id);

    /**
     * 生成结算单
     * @param id
     * @return
     */
    public BusStatement generateStatement(Long id);


    /**
     * 支付
     * @param id
     */
    public void pay(Long id);
}
