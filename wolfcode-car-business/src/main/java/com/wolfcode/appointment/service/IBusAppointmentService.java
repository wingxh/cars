package com.wolfcode.appointment.service;

import java.util.List;
import com.wolfcode.appointment.domain.BusAppointment;
import com.wolfcode.appointment.domain.vo.BusAppointmentVo;

/**
 * 养修信息预约Service接口
 * 
 * @author wolfcode
 * @date 2022-11-20
 */
public interface IBusAppointmentService 
{
    /**
     * 查询养修信息预约
     * 
     * @param id 养修信息预约主键
     * @return 养修信息预约
     */
    public BusAppointment selectBusAppointmentById(Long id);

    /**
     * 查询养修信息预约列表
     * 
     * @param busAppointment 养修信息预约
     * @return 养修信息预约集合
     */
    public List<BusAppointment> selectBusAppointmentList(BusAppointment busAppointment);

    /**
     * 新增养修信息预约
     * 
     * @param BusAppointmentVo 养修信息预约
     * @return 结果
     */
    public int insertBusAppointment(BusAppointmentVo busAppointmentVo);

    /**
     * 修改养修信息预约
     * 
     * @param busAppointment 养修信息预约
     * @return 结果
     */
    public int updateBusAppointment(BusAppointmentVo busAppointmentVO);

    /**
     * 批量删除养修信息预约
     * 
     * @param ids 需要删除的养修信息预约主键集合
     * @return 结果
     */
    public int deleteBusAppointmentByIds(Long[] ids);

    /**
     * 删除养修信息预约信息
     * 
     * @param id 养修信息预约主键
     * @return 结果
     */
    public int deleteBusAppointmentById(Long id);


    /**
     *
     *  预约取消
     * @param id 养修信息预约主键
     * @return 结果
     */
    public int cancel(Long id);

    /**
     *
     *  到店
     * @param id 养修信息预约主键
     * @return 结果
     */
    public int arrive(Long id);
}
