package com.wolfcode.appointment.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.wolfcode.appointment.domain.vo.BusAppointmentVo;
import com.wolfcode.appointment.service.IBusServiceItemService;
import com.wolfcode.appointment.service.IBusStatementService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wolfcode.common.annotation.Log;
import com.wolfcode.common.core.controller.BaseController;
import com.wolfcode.common.core.domain.AjaxResult;
import com.wolfcode.common.enums.BusinessType;
import com.wolfcode.appointment.domain.BusAppointment;
import com.wolfcode.appointment.service.IBusAppointmentService;
import com.wolfcode.common.utils.poi.ExcelUtil;
import com.wolfcode.common.core.page.TableDataInfo;

/**
 * 养修信息预约Controller
 * 
 * @author wolfcode
 * @date 2022-11-20
 */
@RestController
@RequestMapping("/appointment/appointment")
public class BusAppointmentController extends BaseController
{
    @Autowired
    private IBusAppointmentService busAppointmentService;
    @Autowired
    private IBusStatementService statementService;
    /**
     * 查询养修信息预约列表
     */
    @PreAuthorize("@ss.hasPermi('appointment:appointment:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusAppointment busAppointment)
    {
        startPage();
        List<BusAppointment> list = busAppointmentService.selectBusAppointmentList(busAppointment);
        return getDataTable(list);
    }

    /**
     * 获取养修信息预约详细信息
     */
    @PreAuthorize("@ss.hasPermi('appointment:appointment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(busAppointmentService.selectBusAppointmentById(id));
    }

    /**
     * 新增养修信息预约
     */
    @PreAuthorize("@ss.hasPermi('appointment:appointment:add')")
    @Log(title = "养修信息预约", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusAppointmentVo busAppointmentVo)
    {
        return toAjax(busAppointmentService.insertBusAppointment(busAppointmentVo));
    }

    /**
     * 修改养修信息预约
     */
    @PreAuthorize("@ss.hasPermi('appointment:appointment:edit')")
    @Log(title = "养修信息预约", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusAppointmentVo busAppointmentVo)
    {
        return toAjax(busAppointmentService.updateBusAppointment(busAppointmentVo));
    }

    /**
     * 删除养修信息预约
     */
    @PreAuthorize("@ss.hasPermi('appointment:appointment:remove')")
    @Log(title = "养修信息预约", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(busAppointmentService.deleteBusAppointmentByIds(ids));
    }

    /**
     * 预约信息取消
     */
    @PreAuthorize("@ss.hasPermi('appointment:appointment:cancel')")
    @Log(title = "养修信息预约", businessType = BusinessType.UPDATE)
    @PutMapping("cancel/{id}")
    public AjaxResult cancel(@PathVariable Long id)
    {
        return toAjax(busAppointmentService.cancel(id));
    }


    /**
     * 到店
     */
    @PreAuthorize("@ss.hasPermi('appointment:appointment:cancel')")
    @Log(title = "养修信息预约", businessType = BusinessType.UPDATE)
    @PutMapping("arrive/{id}")
    public AjaxResult arrive(@PathVariable Long id)
    {
        return toAjax(busAppointmentService.arrive(id));
    }

    /**
     * 新增养修信息预约
     */
    @PreAuthorize("@ss.hasPermi('appointment:appointment:generate')")
    @Log(title = "养修信息预约", businessType = BusinessType.INSERT)
    @PostMapping("generateStatement/{id}")
    public AjaxResult generateStatement(@PathVariable Long id)
    {
        return AjaxResult.success((statementService.generateStatement(id)));
    }
}
