package com.wolfcode.appointment.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.wolfcode.appointment.domain.vo.BusStatementAndStatementItemsVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wolfcode.common.annotation.Log;
import com.wolfcode.common.core.controller.BaseController;
import com.wolfcode.common.core.domain.AjaxResult;
import com.wolfcode.common.enums.BusinessType;
import com.wolfcode.appointment.domain.BusStatementItem;
import com.wolfcode.appointment.service.IBusStatementItemService;
import com.wolfcode.common.utils.poi.ExcelUtil;
import com.wolfcode.common.core.page.TableDataInfo;

/**
 * 结算单明细Controller
 * 
 * @author wolfcode
 * @date 2022-11-25
 */
@RestController
@RequestMapping("/appointment/statement/item")
public class BusStatementItemController extends BaseController
{
    @Autowired
    private IBusStatementItemService busStatementItemService;

    /**
     * 查询结算单明细列表
     */
    @PreAuthorize("@ss.hasPermi('appointment:item:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusStatementItem busStatementItem)
    {
        startPage();
        List<BusStatementItem> list = busStatementItemService.selectBusStatementItemList(busStatementItem);
        return getDataTable(list);
    }


    /**
     * 获取结算单明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('appointment:item:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(busStatementItemService.selectBusStatementItemById(id));
    }

    /**
     * 新增结算单明细
     */
    @PreAuthorize("@ss.hasPermi('appointment:item:add')")
    @Log(title = "结算单明细", businessType = BusinessType.INSERT)
    @PostMapping
    public void add(@RequestBody BusStatementAndStatementItemsVo busStatementAndStatementItemsVo)
    {
        busStatementItemService.save(busStatementAndStatementItemsVo);
    }

    /**
     * 修改结算单明细
     */
    @PreAuthorize("@ss.hasPermi('appointment:item:edit')")
    @Log(title = "结算单明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusStatementItem busStatementItem)
    {
        return toAjax(busStatementItemService.updateBusStatementItem(busStatementItem));
    }

    /**
     * 删除结算单明细
     */
    @PreAuthorize("@ss.hasPermi('appointment:item:remove')")
    @Log(title = "结算单明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(busStatementItemService.deleteBusStatementItemByIds(ids));
    }
}
