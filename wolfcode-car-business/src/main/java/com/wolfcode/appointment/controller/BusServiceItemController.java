package com.wolfcode.appointment.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.wolfcode.appointment.domain.info.ServiceItemAuditInfo;
import com.wolfcode.appointment.domain.vo.BusServiceItemAuditVo;
import com.wolfcode.appointment.domain.vo.BusServiceItemVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wolfcode.common.annotation.Log;
import com.wolfcode.common.core.controller.BaseController;
import com.wolfcode.common.core.domain.AjaxResult;
import com.wolfcode.common.enums.BusinessType;
import com.wolfcode.appointment.domain.BusServiceItem;
import com.wolfcode.appointment.service.IBusServiceItemService;
import com.wolfcode.common.utils.poi.ExcelUtil;
import com.wolfcode.common.core.page.TableDataInfo;

/**
 * 服务项Controller
 * 
 * @author wolfcode
 * @date 2022-11-22
 */
@RestController
@RequestMapping("/appointment/item")
public class BusServiceItemController extends BaseController
{
    @Autowired
    private IBusServiceItemService busServiceItemService;

    /**
     * 查询服务项列表
     */
    @PreAuthorize("@ss.hasPermi('appointment:item:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusServiceItem busServiceItem)
    {
        startPage();
        List<BusServiceItem> list = busServiceItemService.selectBusServiceItemList(busServiceItem);
        return getDataTable(list);
    }

    /**
     * 导出服务项列表
     */
    @PreAuthorize("@ss.hasPermi('appointment:item:export')")
    @Log(title = "服务项", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusServiceItem busServiceItem)
    {
        List<BusServiceItem> list = busServiceItemService.selectBusServiceItemList(busServiceItem);
        ExcelUtil<BusServiceItem> util = new ExcelUtil<BusServiceItem>(BusServiceItem.class);
        util.exportExcel(response, list, "服务项数据");
    }

    /**
     * 获取服务项详细信息
     */
    @PreAuthorize("@ss.hasPermi('appointment:item:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(busServiceItemService.selectBusServiceItemById(id));
    }

    /**
     * 新增服务项
     */
    @PreAuthorize("@ss.hasPermi('appointment:item:add')")
    @Log(title = "服务项", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusServiceItemVo busServiceItemVo)
    {
        return toAjax(busServiceItemService.insertBusServiceItem(busServiceItemVo));
    }

    /**
     * 修改服务项
     */
    @PreAuthorize("@ss.hasPermi('appointment:item:edit')")
    @Log(title = "服务项", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusServiceItemVo busServiceItemVo)
    {
        return toAjax(busServiceItemService.updateBusServiceItem(busServiceItemVo));
    }

    /**
     * 删除服务项
     */
    @PreAuthorize("@ss.hasPermi('appointment:item:remove')")
    @Log(title = "服务项", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(busServiceItemService.deleteBusServiceItemByIds(ids));
    }

    /**
     * 上架
     * @param id
     * @return
     */
    @PreAuthorize("@ss.hasPermi('appointment:item:saleOn')")
    @Log(title = "服务项", businessType = BusinessType.UPDATE)
    @PutMapping("saleOn/{id}")
    public void saleOn(@PathVariable Long id)
    {
        busServiceItemService.saleOn(id);
    }


    /**
     * 下架
     * @param id
     * @return
     */
    @PreAuthorize("@ss.hasPermi('appointment:item:saleOn')")
    @Log(title = "服务项", businessType = BusinessType.UPDATE)
    @PutMapping("saleOff/{id}")
    public void saleOff(@PathVariable Long id)
    {
        busServiceItemService.saleOff(id);
    }

    @GetMapping("/audit/{id}")
    @PreAuthorize("@ss.hasPermi('business:serviceItem:audit')")
    public AjaxResult auditInfo(@PathVariable Long id){
        ServiceItemAuditInfo auditInfo = busServiceItemService.auditInfo(id);
        return AjaxResult.success(auditInfo);
    }

    @PutMapping("/audit")
    @PreAuthorize("@ss.hasPermi('business:serviceItem:startAudit')")
    public AjaxResult startAudit(@RequestBody BusServiceItemAuditVo auditVo){
        busServiceItemService.startAudit(auditVo);
        return AjaxResult.success();
    }
}
