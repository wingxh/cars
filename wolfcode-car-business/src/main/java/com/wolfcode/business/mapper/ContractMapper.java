package com.wolfcode.business.mapper;

import java.util.List;
import com.wolfcode.business.domain.Contract;
import com.wolfcode.business.domain.qo.ContractQo;

/**
 * 合同信息Mapper接口
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
public interface ContractMapper 
{
    /**
     * 查询合同信息
     * 
     * @param id 合同信息主键
     * @return 合同信息
     */
    public Contract selectContractById(Long id);

    /**
     * 查询合同信息列表
     * 
     * @param contractQo 合同信息
     * @return 合同信息集合
     */
    public List<Contract> selectContractList(ContractQo contractQo);

    /**
     * 新增合同信息
     * 
     * @param contract 合同信息
     * @return 结果
     */
    public int insertContract(Contract contract);

    /**
     * 修改合同信息
     * 
     * @param contract 合同信息
     * @return 结果
     */
    public int updateContract(Contract contract);

    /**
     * 删除合同信息
     * 
     * @param id 合同信息主键
     * @return 结果
     */
    public int deleteContractById(Long id);

    /**
     * 批量删除合同信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteContractByIds(Long[] ids);
}
