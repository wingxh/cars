package com.wolfcode.business.enums;

public enum ContractAuditEnum {
    // 未审核
    Unapproved,
    // 审核通过
    PASS,
    // 审核不通过
    REJECT
}
