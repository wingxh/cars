package com.wolfcode.business.service;

import java.util.List;
import com.wolfcode.business.domain.Contract;
import com.wolfcode.business.domain.qo.ContractQo;
import com.wolfcode.business.domain.vo.ContractVo;

/**
 * 合同信息Service接口
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
public interface IContractService 
{
    /**
     * 查询合同信息
     * 
     * @param id 合同信息主键
     * @return 合同信息
     */
    public Contract selectContractById(Long id);

    /**
     * 查询合同信息列表
     * 
     * @param contractQo 合同信息
     * @return 合同信息集合
     */
    public List<Contract> selectContractList(ContractQo contractQo);

    /**
     * 新增合同信息
     * 
     * @param contractVo 合同信息
     * @return 结果
     */
    public int insertContract(ContractVo contractVo) throws Exception;

    /**
     * 修改合同信息
     * 
     * @param contractVo 合同信息
     * @return 结果
     */
    public int updateContract(ContractVo contractVo) throws Exception;

    /**
     * 批量删除合同信息
     * 
     * @param ids 需要删除的合同信息主键集合
     * @return 结果
     */
    public int deleteContractByIds(Long[] ids);

    /**
     * 删除合同信息信息
     * 
     * @param id 合同信息主键
     * @return 结果
     */
    public int deleteContractById(Long id);

    /**
     * 合同作废
     *
     * @param id 合同信息id
     */
    void cancelContract(Long id);

    /**
     * 审核通过
     *
     * @param id 合同信息id
     */
    void auditSuccess(Long id);

    /**
     * 审核不通过
     *
     * @param id 合同信息id
     */
    void auditFail(Long id);

    /**
     * 确认盖章
     *
     * @param id 合同信息id
     */
    void handleSeal(Long id);
}
