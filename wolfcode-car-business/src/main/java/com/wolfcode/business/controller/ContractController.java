package com.wolfcode.business.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.wolfcode.business.domain.qo.ContractQo;
import com.wolfcode.business.domain.vo.ContractVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wolfcode.common.annotation.Log;
import com.wolfcode.common.core.controller.BaseController;
import com.wolfcode.common.core.domain.AjaxResult;
import com.wolfcode.common.enums.BusinessType;
import com.wolfcode.business.domain.Contract;
import com.wolfcode.business.service.IContractService;
import com.wolfcode.common.utils.poi.ExcelUtil;
import com.wolfcode.common.core.page.TableDataInfo;

/**
 * 合同信息Controller
 *
 * @author wolfcode
 * @date 2022-12-10
 */
@RestController
@RequestMapping("/business/contract")
public class ContractController extends BaseController {
    @Autowired
    private IContractService contractService;

    /**
     * 查询合同信息列表
     */
    @PreAuthorize("@ss.hasPermi('business:contract:list')")
    @GetMapping("/list")
    public TableDataInfo list(ContractQo contractQo) {
        startPage();
        List<Contract> list = contractService.selectContractList(contractQo);
        return getDataTable(list);
    }

    /**
     * 导出合同信息列表
     */
    @PreAuthorize("@ss.hasPermi('business:contract:export')")
    @Log(title = "合同信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ContractQo contractQo) {
        List<Contract> list = contractService.selectContractList(contractQo);
        ExcelUtil<Contract> util = new ExcelUtil<Contract>(Contract.class);
        util.exportExcel(response, list, "合同信息数据");
    }

    /**
     * 获取合同信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('business:contract:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(contractService.selectContractById(id));
    }

    /**
     * 新增合同信息
     */
    @PreAuthorize("@ss.hasPermi('business:contract:add')")
    @Log(title = "合同信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(ContractVo contractVo) throws Exception {
        return toAjax(contractService.insertContract(contractVo));
    }

    /**
     * 编辑合同信息
     */
    @PreAuthorize("@ss.hasPermi('business:contract:edit')")
    @Log(title = "合同信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(ContractVo contractVo) throws Exception {
        return toAjax(contractService.updateContract(contractVo));
    }

    /**
     * 合同作废
     */
    @PreAuthorize("@ss.hasPermi('business:contract:cancel')")
    @Log(title = "合同信息", businessType = BusinessType.UPDATE)
    @PutMapping("/cancel/{id}")
    public void cancelContract(@PathVariable Long id) {
        contractService.cancelContract(id);
    }

    /**
     * 审核通过
     */
    @PreAuthorize("@ss.hasPermi('business:contract:auditSuccess')")
    @Log(title = "合同信息", businessType = BusinessType.UPDATE)
    @PutMapping("/auditSuccess/{id}")
    public void auditSuccess(@PathVariable Long id) {
        contractService.auditSuccess(id);
    }

    /**
     * 审核不通过
     */
    @PreAuthorize("@ss.hasPermi('business:contract:auditFail')")
    @Log(title = "合同信息", businessType = BusinessType.UPDATE)
    @PutMapping("/auditFail/{id}")
    public void auditFail(@PathVariable Long id) {
        contractService.auditFail(id);
    }

    /**
     * 确认盖章
     */
    @PreAuthorize("@ss.hasPermi('business:contract:handleSeal')")
    @Log(title = "合同信息", businessType = BusinessType.UPDATE)
    @PutMapping("/handleSeal/{id}")
    public void handleSeal(@PathVariable Long id) {
        contractService.handleSeal(id);
    }

}
