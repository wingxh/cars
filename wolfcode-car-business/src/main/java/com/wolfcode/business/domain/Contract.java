package com.wolfcode.business.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;

/**
 * 合同信息对象 contract
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
@Data
public class Contract extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 客户id */
    @Excel(name = "客户id")
    private Long customerId;

    /** 客户名称 */
    @Excel(name = "客户名称")
    private String customerName;

    /** 合同名称 */
    @Excel(name = "合同名称")
    private String contractName;

    /** 合同编码 */
    @Excel(name = "合同编码")
    private String contractCode;

    /** 合同金额 */
    @Excel(name = "合同金额")
    private BigDecimal amounts;

    /** 合同生效开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "合同生效开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startDate;

    /** 合同生效结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "合同生效结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endDate;

    /** 附件 */
    @Excel(name = "附件")
    private String appendix;

    /** 是否盖章确认 0 否 1 是 */
    @Excel(name = "是否盖章确认 0 否 1 是")
    private Integer affixSealState;

    /** 审核状态 0 未审核 1 审核通过 -1 审核不通过 */
    @Excel(name = "审核状态 0 待审核 1 审核通过 2 审核不通过")
    private Integer auditState;

    /** 是否作废 -1 作废 0 在用 */
    @Excel(name = "是否作废 0 作废 1 在用")
    private Integer nullifyState;

    /** 录入人 */
    private Long inputUser;

    /** 录入人名称 */
    @Excel(name = "录入人名称")
    private String username;

    /** 录入时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "录入时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date inputTime;
}
