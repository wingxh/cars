package com.wolfcode.business.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

/**
 * 合同信息对象 contractVo
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
@Data
public class ContractVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 客户id */
    private Long customerId;

    /** 合同名称 */
    private String contractName;

    /** 合同编码 */
    private String contractCode;

    /** 合同金额 */
    private BigDecimal amounts;

    public void setAmounts(BigDecimal amounts) {
        // 设置FTP
        amounts.setScale(2, RoundingMode.DOWN);
        this.amounts = amounts;
    }

    /** 合同生效开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    /** 合同生效结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    /** 附件 */
    private MultipartFile appendix;


}
