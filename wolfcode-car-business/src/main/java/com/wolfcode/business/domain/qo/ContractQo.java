package com.wolfcode.business.domain.qo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 合同信息查询对象 contractQo
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
@Data
public class ContractQo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 关键字 */
    private String keyWord;

    /** 审核状态 */
    private Integer auditState;

    /** 客户名称 */
    private String customerName;
}
