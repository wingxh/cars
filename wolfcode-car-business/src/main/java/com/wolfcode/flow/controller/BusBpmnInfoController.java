package com.wolfcode.flow.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wolfcode.common.annotation.Log;
import com.wolfcode.common.core.controller.BaseController;
import com.wolfcode.common.core.domain.AjaxResult;
import com.wolfcode.common.enums.BusinessType;
import com.wolfcode.flow.domain.BusBpmnInfo;
import com.wolfcode.flow.service.IBusBpmnInfoService;
import com.wolfcode.common.utils.poi.ExcelUtil;
import com.wolfcode.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 流程定义明细Controller
 * 
 * @author wolfcode
 * @date 2022-11-28
 */
@RestController
@RequestMapping("/flow/info")
public class BusBpmnInfoController extends BaseController
{
    @Autowired
    private IBusBpmnInfoService busBpmnInfoService;

    /**
     * 查询流程定义明细列表
     */
    @PreAuthorize("@ss.hasPermi('flow:info:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusBpmnInfo busBpmnInfo)
    {
        startPage();
        List<BusBpmnInfo> list = busBpmnInfoService.selectBusBpmnInfoList(busBpmnInfo);
        return getDataTable(list);
    }

    /**
     * 导出流程定义明细列表
     */
    @PreAuthorize("@ss.hasPermi('flow:info:export')")
    @Log(title = "流程定义明细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusBpmnInfo busBpmnInfo)
    {
        List<BusBpmnInfo> list = busBpmnInfoService.selectBusBpmnInfoList(busBpmnInfo);
        ExcelUtil<BusBpmnInfo> util = new ExcelUtil<BusBpmnInfo>(BusBpmnInfo.class);
        util.exportExcel(response, list, "流程定义明细数据");
    }

    /**
     * 获取流程定义明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('flow:info:query')")
    @GetMapping(value = "/{type}/{id}")
    public void getInfo(@PathVariable("type") String type,
                        @PathVariable("id") Long id,
                        HttpServletResponse response) throws IOException {
        InputStream is = busBpmnInfoService.getResource(type, id);
        // xml流还是图片流
        IOUtils.copy(is,response.getOutputStream());
    }

    /**
     * 新增流程定义明细
     */
    @PreAuthorize("@ss.hasPermi('flow:info:add')")
    @Log(title = "流程定义明细", businessType = BusinessType.INSERT)
    @PostMapping
    public void add(MultipartFile file,String bpmnLabel,
                          Integer bpmnType,String info) throws IOException
    {
        busBpmnInfoService.deploy(file,bpmnLabel,bpmnType,info);
    }

    /**
     * 修改流程定义明细
     */
    @PreAuthorize("@ss.hasPermi('flow:info:edit')")
    @Log(title = "流程定义明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusBpmnInfo busBpmnInfo)
    {
        return toAjax(busBpmnInfoService.updateBusBpmnInfo(busBpmnInfo));
    }

    /**
     * 删除流程定义明细
     */
    @PreAuthorize("@ss.hasPermi('flow:info:remove')")
    @Log(title = "流程定义明细", businessType = BusinessType.DELETE)
	@DeleteMapping(value ="/{id}")
    public AjaxResult remove(@PathVariable Long id)
    {
        return toAjax(busBpmnInfoService.deleteBusBpmnInfoById(id));
    }


}
