package com.wolfcode.goods.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.wolfcode.goods.domain.vo.StoreVo;

import com.wolfcode.goods.domain.info.SelectStoreItems;
import com.wolfcode.goods.domain.vo.StoreListVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wolfcode.common.annotation.Log;
import com.wolfcode.common.core.controller.BaseController;
import com.wolfcode.common.core.domain.AjaxResult;
import com.wolfcode.common.enums.BusinessType;
import com.wolfcode.goods.domain.Store;
import com.wolfcode.goods.service.IStoreService;
import com.wolfcode.common.utils.poi.ExcelUtil;
import com.wolfcode.common.core.page.TableDataInfo;

/**
 * 仓库信息Controller
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
@RestController
@RequestMapping("/goods/store")
public class StoreController extends BaseController
{
    @Autowired
    private IStoreService storeService;

    /**
     * 查询仓库信息列表
     */
    @PreAuthorize("@ss.hasPermi('goods:store:list')")
    @GetMapping("/list")
    public TableDataInfo list(Store store)
    {
        startPage();
        List<Store> list = storeService.selectStoreList(store);
        return getDataTable(list);
    }

    /**
     * 查询仓库列表
     */
    @GetMapping("/all")
    public AjaxResult storeList()
    {
        return AjaxResult.success( storeService.storeList());
    }

    /**
     * 导出仓库信息列表
     */
    @PreAuthorize("@ss.hasPermi('goods:store:export')")
    @Log(title = "仓库信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Store store)
    {
        List<Store> list = storeService.selectStoreList(store);
        ExcelUtil<Store> util = new ExcelUtil<Store>(Store.class);
        util.exportExcel(response, list, "仓库信息数据");
    }

    /**
     * 获取仓库信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:store:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(storeService.selectStoreById(id));
    }

    /**
     * 新增仓库信息
     */
    @PreAuthorize("@ss.hasPermi('goods:store:add')")
    @Log(title = "仓库信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StoreVo storeVo)
    {
        return toAjax(storeService.insertStore(storeVo));
    }

    /**
     * 修改仓库信息
     */
    @PreAuthorize("@ss.hasPermi('goods:store:edit')")
    @Log(title = "仓库信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StoreVo storeVo)
    {
        return toAjax(storeService.updateStore(storeVo));
    }

    /**
     * 删除仓库信息
     */
    @PreAuthorize("@ss.hasPermi('goods:store:remove')")
    @Log(title = "仓库信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{id}")
    public void remove(@PathVariable Long id)
    {
        storeService.deleteStoreById(id);
    }

    /**
     * 根据仓库id查询 可以选择的商品列表
     * */
    @PostMapping("/selectList/{id}")
    public TableDataInfo SelectStoreItems(@RequestBody StoreListVo storeListVo, @PathVariable Long id)
    {

        List<SelectStoreItems> selectStoreItems = storeService.SelectStoreItems(storeListVo, id);
        return getDataTable(selectStoreItems);
    }

}
