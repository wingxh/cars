package com.wolfcode.goods.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.wolfcode.goods.domain.Store;
import com.wolfcode.goods.domain.info.GoodsInfo;
import com.wolfcode.goods.domain.info.StockRecord;
import com.wolfcode.goods.domain.qo.GoodsQo;
import com.wolfcode.goods.domain.vo.GoodsVo;
import com.wolfcode.goods.mapper.StoreMapper;
import com.wolfcode.goods.service.IStoreService;
import org.checkerframework.checker.units.qual.A;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wolfcode.common.annotation.Log;
import com.wolfcode.common.core.controller.BaseController;
import com.wolfcode.common.core.domain.AjaxResult;
import com.wolfcode.common.enums.BusinessType;
import com.wolfcode.goods.domain.Goods;
import com.wolfcode.goods.service.IGoodsService;
import com.wolfcode.common.utils.poi.ExcelUtil;
import com.wolfcode.common.core.page.TableDataInfo;

/**
 * 物品信息Controller
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
@RestController
@RequestMapping("/goods/goods")
public class GoodsController extends BaseController
{
    @Autowired
    private IGoodsService goodsService;
    @Autowired
    private IStoreService storeService;

    /**
     * 查询物品信息列表
     */
    @PreAuthorize("@ss.hasPermi('goods:goods:list')")
    @GetMapping("/list")
    public TableDataInfo list(GoodsQo goodsQo)
    {
        startPage();
        List<GoodsInfo> list = goodsService.selectGoodsList(goodsQo);
        return getDataTable(list);
    }

    /**
     * 获取物品信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:goods:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(goodsService.selectGoodsById(id));
    }

    /**
     * 新增物品信息
     */
    @PreAuthorize("@ss.hasPermi('goods:goods:add')")
    @Log(title = "物品信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add( @Valid GoodsVo goodsVo) throws Exception {
        return toAjax(goodsService.insertGoods(goodsVo));
    }

    /**
     * 修改物品信息
     */
    @PreAuthorize("@ss.hasPermi('goods:goods:edit')")
    @Log(title = "物品信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Valid GoodsVo goodsVo) throws Exception {
        return toAjax(goodsService.updateGoods(goodsVo));
    }

    /**
     * 删除物品信息
     */
    @PreAuthorize("@ss.hasPermi('goods:goods:remove')")
    @Log(title = "物品信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(goodsService.deleteGoodsByIds(ids));
    }

    @PreAuthorize("@ss.hasPermi('goods:goods:stores')")
    @GetMapping("/stores")
    public TableDataInfo stores()
    {
        List<Store> list = storeService.getStoreList();
        return getDataTable(list);
    }

    @PreAuthorize("@ss.hasPermi('goods:goods:list')")
    @GetMapping("/records/{id}")
    public AjaxResult listStockRecord(@PathVariable Long id)
    {
        List<StockRecord> list = goodsService.selectStockRecordsByGoodsId(id);
        return AjaxResult.success(list);
    }
}
