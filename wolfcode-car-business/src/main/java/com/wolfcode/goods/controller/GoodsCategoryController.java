package com.wolfcode.goods.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.wolfcode.common.core.domain.entity.SysDept;
import com.wolfcode.goods.domain.vo.GoodsCategoryVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wolfcode.common.annotation.Log;
import com.wolfcode.common.core.controller.BaseController;
import com.wolfcode.common.core.domain.AjaxResult;
import com.wolfcode.common.enums.BusinessType;
import com.wolfcode.goods.domain.GoodsCategory;
import com.wolfcode.goods.service.IGoodsCategoryService;
import com.wolfcode.common.utils.poi.ExcelUtil;
import com.wolfcode.common.core.page.TableDataInfo;

/**
 * 物品分类信息Controller
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
@RestController
@RequestMapping("/goods/category")
public class GoodsCategoryController extends BaseController
{
    @Autowired
    private IGoodsCategoryService goodsCategoryService;

    /**
     * 查询物品分类信息列表
     */
    @PreAuthorize("@ss.hasPermi('goods:category:list')")
    @GetMapping("/list")
    public TableDataInfo list(GoodsCategory goodsCategory)
    {
        startPage();
        List<GoodsCategory> list = goodsCategoryService.selectGoodsCategoryList(goodsCategory);
        return getDataTable(list);
    }

    /**
     * 获取商品分类下拉树列表
     */
    @GetMapping("/treeselect")
    public AjaxResult treeselect(GoodsCategory category)
    {
        List<GoodsCategory> categories = goodsCategoryService.selectGoodsCategoryList(category);
        return AjaxResult.success(goodsCategoryService.buildTreeSelect(categories));
    }

    /**
     * 导出物品分类信息列表
     */
    @PreAuthorize("@ss.hasPermi('goods:category:export')")
    @Log(title = "物品分类信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, GoodsCategory goodsCategory)
    {
        List<GoodsCategory> list = goodsCategoryService.selectGoodsCategoryList(goodsCategory);
        ExcelUtil<GoodsCategory> util = new ExcelUtil<GoodsCategory>(GoodsCategory.class);
        util.exportExcel(response, list, "物品分类信息数据");
    }

    /**
     * 获取物品分类信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:category:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(goodsCategoryService.selectGoodsCategoryById(id));
    }

    /**
     * 新增物品分类信息
     */
    @PreAuthorize("@ss.hasPermi('goods:category:add')")
    @Log(title = "物品分类信息", businessType = BusinessType.INSERT)
    @PostMapping
    public void add(@RequestBody @Valid GoodsCategoryVo categoryVo)
    {
        goodsCategoryService.insertGoodsCategory(categoryVo);
    }

    /**
     * 修改物品分类信息
     */
    @PreAuthorize("@ss.hasPermi('goods:category:edit')")
    @Log(title = "物品分类信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public void edit(@RequestBody @Valid GoodsCategoryVo goodsCategoryVo)
    {
        goodsCategoryService.updateGoodsCategory(goodsCategoryVo);
    }

    /**
     * 删除物品分类信息
     */
    @PreAuthorize("@ss.hasPermi('goods:category:remove')")
    @Log(title = "物品分类信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{id}")
    public void remove(@PathVariable Long id)
    {
        goodsCategoryService.deleteGoodsCategoryById(id);
    }

    /**
     * 获取分类选择择选项
     * */
    @GetMapping("/select")
    public AjaxResult selectGoodsCategory()
    {
        return AjaxResult.success(goodsCategoryService.selectGoodsCategory());
    }
}
