package com.wolfcode.goods.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wolfcode.common.annotation.Log;
import com.wolfcode.common.core.controller.BaseController;
import com.wolfcode.common.core.domain.AjaxResult;
import com.wolfcode.common.enums.BusinessType;
import com.wolfcode.goods.domain.StockBillItem;
import com.wolfcode.goods.service.IStockBillItemService;
import com.wolfcode.common.utils.poi.ExcelUtil;
import com.wolfcode.common.core.page.TableDataInfo;

/**
 * 出入库单据明细Controller
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
@RestController
@RequestMapping("/goods/item")
public class StockBillItemController extends BaseController
{
    @Autowired
    private IStockBillItemService stockBillItemService;

    /**
     * 查询出入库单据明细列表
     */
    @PreAuthorize("@ss.hasPermi('goods:item:list')")
    @GetMapping("/list")
    public TableDataInfo list(StockBillItem stockBillItem)
    {
        startPage();
        List<StockBillItem> list = stockBillItemService.selectStockBillItemList(stockBillItem);
        return getDataTable(list);
    }

    /**
     * 导出出入库单据明细列表
     */
    @PreAuthorize("@ss.hasPermi('goods:item:export')")
    @Log(title = "出入库单据明细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StockBillItem stockBillItem)
    {
        List<StockBillItem> list = stockBillItemService.selectStockBillItemList(stockBillItem);
        ExcelUtil<StockBillItem> util = new ExcelUtil<StockBillItem>(StockBillItem.class);
        util.exportExcel(response, list, "出入库单据明细数据");
    }

    /**
     * 获取出入库单据明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:item:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(stockBillItemService.selectStockBillItemById(id));
    }

    /**
     * 新增出入库单据明细
     */
    @PreAuthorize("@ss.hasPermi('goods:item:add')")
    @Log(title = "出入库单据明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StockBillItem stockBillItem)
    {
        return toAjax(stockBillItemService.insertStockBillItem(stockBillItem));
    }

    /**
     * 修改出入库单据明细
     */
    @PreAuthorize("@ss.hasPermi('goods:item:edit')")
    @Log(title = "出入库单据明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StockBillItem stockBillItem)
    {
        return toAjax(stockBillItemService.updateStockBillItem(stockBillItem));
    }

    /**
     * 删除出入库单据明细
     */
    @PreAuthorize("@ss.hasPermi('goods:item:remove')")
    @Log(title = "出入库单据明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(stockBillItemService.deleteStockBillItemByIds(ids));
    }
}
