package com.wolfcode.goods.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wolfcode.common.annotation.Log;
import com.wolfcode.common.core.controller.BaseController;
import com.wolfcode.common.core.domain.AjaxResult;
import com.wolfcode.common.enums.BusinessType;
import com.wolfcode.goods.domain.GoodsStore;
import com.wolfcode.goods.service.IGoodsStoreService;
import com.wolfcode.common.utils.poi.ExcelUtil;
import com.wolfcode.common.core.page.TableDataInfo;

/**
 * 物品库存Controller
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
@RestController
@RequestMapping("/goods/goodsStore")
public class GoodsStoreController extends BaseController
{
    @Autowired
    private IGoodsStoreService goodsStoreService;

    /**
     * 查询物品库存列表
     */
    @PreAuthorize("@ss.hasPermi('goods:goodsStore:list')")
    @GetMapping("/list")
    public TableDataInfo list(GoodsStore goodsStore)
    {
        startPage();
        List<GoodsStore> list = goodsStoreService.selectGoodsStoreList(goodsStore);
        return getDataTable(list);
    }

    /**
     * 导出物品库存列表
     */
    @PreAuthorize("@ss.hasPermi('goods:goodsStore:export')")
    @Log(title = "物品库存", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, GoodsStore goodsStore)
    {
        List<GoodsStore> list = goodsStoreService.selectGoodsStoreList(goodsStore);
        ExcelUtil<GoodsStore> util = new ExcelUtil<GoodsStore>(GoodsStore.class);
        util.exportExcel(response, list, "物品库存数据");
    }

    /**
     * 获取物品库存详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:goodsStore:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(goodsStoreService.selectGoodsStoreById(id));
    }

    /**
     * 新增物品库存
     */
    @PreAuthorize("@ss.hasPermi('goods:goodsStore:add')")
    @Log(title = "物品库存", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GoodsStore goodsStore)
    {
        return toAjax(goodsStoreService.insertGoodsStore(goodsStore));
    }

    /**
     * 修改物品库存
     */
    @PreAuthorize("@ss.hasPermi('goods:goodsStore:edit')")
    @Log(title = "物品库存", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GoodsStore goodsStore)
    {
        return toAjax(goodsStoreService.updateGoodsStore(goodsStore));
    }

    /**
     * 删除物品库存
     */
    @PreAuthorize("@ss.hasPermi('goods:goodsStore:remove')")
    @Log(title = "物品库存", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(goodsStoreService.deleteGoodsStoreByIds(ids));
    }
}
