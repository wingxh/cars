package com.wolfcode.goods.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.wolfcode.goods.domain.info.StockBillInfo;
import com.wolfcode.goods.domain.vo.StockBillVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wolfcode.common.annotation.Log;
import com.wolfcode.common.core.controller.BaseController;
import com.wolfcode.common.core.domain.AjaxResult;
import com.wolfcode.common.enums.BusinessType;
import com.wolfcode.goods.domain.StockBill;
import com.wolfcode.goods.service.IStockBillService;
import com.wolfcode.common.utils.poi.ExcelUtil;
import com.wolfcode.common.core.page.TableDataInfo;

/**
 * 出入库单据Controller
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
@RestController
@RequestMapping("/goods/bill")
public class StockBillController extends BaseController
{
    @Autowired
    private IStockBillService stockBillService;

    /**
     * 查询出入库单据列表 需要包含 总数量总金额和总数量
     */
    @PreAuthorize("@ss.hasPermi('goods:bill:list')")
    @GetMapping("/list")
    public TableDataInfo list(StockBill stockBill)
    {
        startPage();
        List<StockBillInfo> list = stockBillService.selectStockBillList(stockBill);
        return getDataTable(list);
    }

    /**
     * 导出出入库单据列表
     */
    /*@PreAuthorize("@ss.hasPermi('goods:bill:export')")
    @Log(title = "出入库单据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, StockBill stockBill)
    {
        List<StockBill> list = stockBillService.selectStockBillList(stockBill);
        ExcelUtil<StockBill> util = new ExcelUtil<StockBill>(StockBill.class);
        util.exportExcel(response, list, "出入库单据数据");
    }*/

    /**
     * 获取出入库单据详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:bill:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(stockBillService.selectStockBillById(id));
    }

    /**
     * 新增出入库单据
     */
    @PreAuthorize("@ss.hasPermi('goods:bill:add')")
    @Log(title = "出入库单据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StockBillVo stockBillVo)
    {
        return toAjax(stockBillService.insertStockBill(stockBillVo));
    }

    /**
     * 修改出入库单据
     */
    @PreAuthorize("@ss.hasPermi('goods:bill:edit')")
    @Log(title = "出入库单据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StockBill stockBill)
    {
        return toAjax(stockBillService.updateStockBill(stockBill));
    }

    /**
     * 删除出入库单据
     */
    @PreAuthorize("@ss.hasPermi('goods:bill:remove')")
    @Log(title = "出入库单据", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(stockBillService.deleteStockBillByIds(ids));
    }

    /**
     * 查看出入库管理明细
     * */
    @GetMapping("billInfo/{id}")
    public AjaxResult getStockBillInfo(@PathVariable Long id)
    {
        return AjaxResult.success(stockBillService.getStockBillInfo(id));
    }

    /**
     * 作废某个bill以及其上的BillItem
     * */
    @GetMapping(value = "cancel/{id}")
    public AjaxResult cancelBill(@PathVariable("id") Long id)
    {
        return toAjax(stockBillService.cancelBill(id));
    }

}
