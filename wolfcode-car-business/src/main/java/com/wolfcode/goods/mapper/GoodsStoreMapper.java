package com.wolfcode.goods.mapper;

import java.util.List;
import com.wolfcode.goods.domain.GoodsStore;
import org.apache.ibatis.annotations.Param;

/**
 * 物品库存Mapper接口
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
public interface GoodsStoreMapper 
{
    /**
     * 查询物品库存
     * 
     * @param id 物品库存主键
     * @return 物品库存
     */
    public GoodsStore selectGoodsStoreById(Long id);

    /**
     * 查询物品库存列表
     * 
     * @param goodsStore 物品库存
     * @return 物品库存集合
     */
    public List<GoodsStore> selectGoodsStoreList(GoodsStore goodsStore);

    /**
     * 新增物品库存
     * 
     * @param goodsStore 物品库存
     * @return 结果
     */
    public int insertGoodsStore(GoodsStore goodsStore);

    /**
     * 修改物品库存
     * 
     * @param goodsStore 物品库存
     * @return 结果
     */
    public int updateGoodsStore(GoodsStore goodsStore);

    /**
     * 删除物品库存
     * 
     * @param id 物品库存主键
     * @return 结果
     */
    public int deleteGoodsStoreById(Long id);

    /**
     * 批量删除物品库存
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteGoodsStoreByIds(Long[] ids);

    /*
    * 根据goods_id 和 store_id 查询 库存
    * */
    public GoodsStore selectGoodsStoreByGoodsIdAndStoreId(@Param("goodsId") Long goodsId,@Param("storeId") Long storeId);

    /*
     * 根据goods_id 和 store_id 查询 更新库存
     *
     * */
    public int updatetGoodsStoreByGoodsIdAndStoreId(@Param("goodsId") Long goodsId,@Param("storeId") Long storeId,@Param("amounts") Long amounts);
}
