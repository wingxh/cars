package com.wolfcode.goods.mapper;

import java.util.List;

import com.wolfcode.goods.domain.GoodsStore;
import com.wolfcode.goods.domain.Store;
import com.wolfcode.goods.domain.info.SelectStoreItems;
import com.wolfcode.goods.domain.vo.StoreListVo;
import org.apache.ibatis.annotations.Param;

/**
 * 仓库信息Mapper接口
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
public interface StoreMapper 
{
    /**
     * 查询仓库信息
     * 
     * @param id 仓库信息主键
     * @return 仓库信息
     */
    public Store selectStoreById(Long id);

    /**
     * 查询仓库信息列表
     * 
     * @param store 仓库信息
     * @return 仓库信息集合
     */
    public List<Store> selectStoreList(Store store);

    /**
     * 新增仓库信息
     * 
     * @param store 仓库信息
     * @return 结果
     */
    public int insertStore(Store store);

    /**
     * 修改仓库信息
     * 
     * @param store 仓库信息
     * @return 结果
     */
    public int updateStore(Store store);

    /**
     * 删除仓库信息
     * 
     * @param id 仓库信息主键
     * @return 结果
     */
    public int deleteStoreById(Long id);

    public int selectGoodStoreByStoreId(Long id);

    public void deleteGoodStoreByStoreId(Long id);

    /**
     * 批量删除仓库信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStoreByIds(Long[] ids);

    public List<Store> storeList();

    public List<SelectStoreItems> SelectStoreItems(StoreListVo storeListVo);

    /**
     * 获取仓库列表,不分页
     * @return
     */
    List<Store> getStoreList();
}
