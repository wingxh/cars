package com.wolfcode.goods.mapper;

import java.util.List;
import com.wolfcode.goods.domain.StockBill;
import com.wolfcode.goods.domain.info.StockBillInfo;

/**
 * 出入库单据Mapper接口
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
public interface StockBillMapper 
{
    /**
     * 查询出入库单据
     * 
     * @param id 出入库单据主键
     * @return 出入库单据
     */
    public StockBill selectStockBillById(Long id);

    /**
     * 查询出入库单据列表
     * 
     * @param stockBill 出入库单据
     * @return 出入库单据集合
     */
    public List<StockBillInfo> selectStockBillList(StockBill stockBill);

    /**
     * 新增出入库单据
     * 
     * @param stockBill 出入库单据
     * @return 结果
     */
    public int insertStockBill(StockBill stockBill);

    /**
     * 修改出入库单据
     * 
     * @param stockBill 出入库单据
     * @return 结果
     */
    public int updateStockBill(StockBill stockBill);

    /**
     * 删除出入库单据
     * 
     * @param id 出入库单据主键
     * @return 结果
     */
    public int deleteStockBillById(Long id);

    /**
     * 批量删除出入库单据
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteStockBillByIds(Long[] ids);
}
