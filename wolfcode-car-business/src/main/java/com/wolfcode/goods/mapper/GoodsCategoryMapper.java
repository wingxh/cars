package com.wolfcode.goods.mapper;

import java.util.List;
import com.wolfcode.goods.domain.GoodsCategory;
import com.wolfcode.goods.domain.info.SelectCategoryInfo;

/**
 * 物品分类信息Mapper接口
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
public interface GoodsCategoryMapper 
{
    /**
     * 查询物品分类信息
     * 
     * @param id 物品分类信息主键
     * @return 物品分类信息
     */
    public GoodsCategory selectGoodsCategoryById(Long id);

    /**
     * 查询物品分类信息列表
     * 
     * @param goodsCategory 物品分类信息
     * @return 物品分类信息集合
     */
    public List<GoodsCategory> selectGoodsCategoryList(GoodsCategory goodsCategory);
    public List<GoodsCategory> selectGoodsCategoryListById(Long id);

    /**
     * 新增物品分类信息
     * 
     * @param goodsCategory 物品分类信息
     * @return 结果
     */
    public int insertGoodsCategory(GoodsCategory goodsCategory);

    /**
     * 修改物品分类信息
     * 
     * @param goodsCategory 物品分类信息
     * @return 结果
     */
    public int updateGoodsCategory(GoodsCategory goodsCategory);

    /**
     * 删除物品分类信息
     * 
     * @param id 物品分类信息主键
     * @return 结果
     */
    public int deleteGoodsCategoryById(Long id);

    /**
     * 批量删除物品分类信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteGoodsCategoryByIds(Long[] ids);

    public List<SelectCategoryInfo> selectGoodsCategory();
}
