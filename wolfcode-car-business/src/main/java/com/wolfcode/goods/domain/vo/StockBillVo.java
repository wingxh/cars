package com.wolfcode.goods.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.goods.domain.StockBillItem;
import lombok.Data;

import java.util.Date;
import java.util.List;


/** 出入库接受类 */
@Data
public class StockBillVo {

    /** 仓库id */
    private Long storeId;

    /** 出入库时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date busiDate;

    /** 备注 */
    private String remark;

    /** 类型, 0 入库, 1 出库 */
    private Integer type;

    /** 明细项 */
    private List<StockBillItem> stockBillItems;
}
