package com.wolfcode.goods.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;

/**
 * 仓库信息对象 store
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
@Data
public class Store extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 仓库名字 */
    @Excel(name = "仓库名字")
    private String storeName;

    /** 仓库地址 */
    @Excel(name = "仓库地址", readConverterExp = "$column.readConverterExp()")
    private String storeAddress;

    /** 状态, 0正常 , -1删除 */
    @Excel(name = "状态, 0正常 , -1删除")
    private Integer state;

}
