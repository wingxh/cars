package com.wolfcode.goods.domain.info;

import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * 物品信息对象 goods
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
@Data
public class GoodsInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键*/
    private Long id;

    /** 物品名称 */
    private String goodsName;

    /** 封面 */
    private String goodsCover;

    /** 分类名称 */
    private String categoryName;

    /** 品牌 */
    private String brand;

    /** 规格 */
    private String spec;

    /** 描述 */
    private String goodsDesc;

    /** 数量 */
    private Long amounts;

    /** 分类id */
    private Long categoryId;

}
