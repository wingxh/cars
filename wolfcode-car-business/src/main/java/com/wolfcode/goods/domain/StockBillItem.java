package com.wolfcode.goods.domain;

import java.math.BigDecimal;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;

/**
 * 出入库单据明细对象 stock_bill_item
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
@Data
public class StockBillItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 物品id */
    @Excel(name = "物品id")
    private Long goodsId;

    /** 物品id */
    private String goodsName;

    /** 数量 */
    @Excel(name = "数量")
    private Long amounts;

    /** 单价 */
    @Excel(name = "单价")
    private BigDecimal price;

    /** 单据id */
    @Excel(name = "单据id")
    private String billId;

    /** 0 正常, -1 作废 */
    @Excel(name = "0 正常, -1 作废")
    private Integer state;
}
