package com.wolfcode.goods.domain.vo;

import com.wolfcode.common.core.domain.BaseEntity;
import lombok.Data;

@Data
public class StoreListVo extends BaseEntity {
    private Long storeId; // 仓库id
    private Long categoryId;
    private String keywords;
}
