package com.wolfcode.goods.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Size;

/**
 * 物品信息对象 goods
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
@Data
public class Goods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 物品名称 */
    @Excel(name = "物品名称")
    private String goodsName;

    /** 封面 */
    @Excel(name = "封面")
    private String goodsCover;

    /** 分类 */
    @Excel(name = "分类")
    private Long categoryId;

    /** 品牌 */
    @Excel(name = "品牌")
    private String brand;

    /** 规格 */
    @Excel(name = "规格")
    private String spec;

    /** 描述 */
    @Excel(name = "描述")
    private String goodsDesc;

    @Length(min = 1,max = 50,message = "物品名称不能为空")
        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }
        @Length(min = 1,message = "封面不能为空")
        public void setGoodsCover(String goodsCover) {
            this.goodsCover = goodsCover;
        }
        @Length(max = 50,message = "品牌名称不能超过50字")
        public void setBrand(String brand) {
            this.brand = brand;
        }
        @Length(max = 50,message = "规格字数不能大于50")
        public void setSpec(String spec) {
            this.spec = spec;
        }
        @Length(max = 200,message = "描述字数长度不能大于200")
        public void setGoodsDesc(String goodsDesc) {
            this.goodsDesc = goodsDesc;
    }
}
