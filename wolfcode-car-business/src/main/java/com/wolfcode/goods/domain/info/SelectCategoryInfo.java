package com.wolfcode.goods.domain.info;

import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;
import lombok.Data;

@Data
public class SelectCategoryInfo extends BaseEntity {

    /** $column.columnComment */
    private Long id;

    /** 分类名称 */
    @Excel(name = "分类名称")
    private String categoryName;
}
