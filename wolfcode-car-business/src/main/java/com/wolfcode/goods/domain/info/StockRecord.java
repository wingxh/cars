package com.wolfcode.goods.domain.info;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
@Data
public class StockRecord {
    //序号
    private Long id;
    //出入库状态
    private Integer type;
    //操作时间
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date operateDate;
    //物品名称
    private String goodsName;
    //品牌
    private String brand;
    //仓库名
    private String storeName;
    //数量
    private Integer amounts;
    //单价
    private BigDecimal price;
}
