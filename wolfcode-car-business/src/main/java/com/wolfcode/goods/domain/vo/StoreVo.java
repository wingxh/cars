package com.wolfcode.goods.domain.vo;

import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 仓库信息对象 store
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
public class StoreVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 仓库名字 */
    @Excel(name = "仓库名字")
    private String storeName;

    /** 仓库地址 */
    @Excel(name = "仓库地址", readConverterExp = "$column.readConverterExp()")
    private String storeAddress;


    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setStoreName(String storeName) 
    {
        this.storeName = storeName;
    }

    public String getStoreName() 
    {
        return storeName;
    }
    public void setStoreAddress(String storeAddress) 
    {
        this.storeAddress = storeAddress;
    }

    public String getStoreAddress() 
    {
        return storeAddress;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("storeName", getStoreName())
            .append("storeAddress", getStoreAddress())
            .toString();
    }
}
