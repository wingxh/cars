package com.wolfcode.goods.domain.vo;

import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.constraints.NotNull;

@Data
public class GoodsVo {
        /** $column.columnComment */
        private Long id;

        /** 物品名称 */
        @Length(min = 1,max = 50,message = "物品名称不能为空")
        private String goodsName;

        /** 封面 */
        private MultipartFile goodsCover;

        /** 分类 */
        private Long categoryId;

        /** 品牌 */
        @Length(max = 50,message = "品牌名称不能超过50字")
        private String brand;

        /** 规格 */
        @Length(max = 50,message = "规格字数不能大于50")
        private String spec;

        /** 描述 */
        @Length(max = 200,message = "描述字数长度不能大于200")
        private String goodsDesc;

}
