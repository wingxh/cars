package com.wolfcode.goods.domain.qo;

import lombok.Data;

@Data
public class GoodsQo {
    /**
     * 关键字
     */
    private String keyword;

    /**
     *分类id
     */
    private Long categoryId;

    /**
     * 仓库id
     */
    private Long storeId;
}
