package com.wolfcode.goods.domain.vo;

import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;
import com.wolfcode.goods.domain.GoodsCategory;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * 物品分类信息对象 goods_category
 *
 * @author wolfcode
 * @date 2022-12-10
 */
public class GoodsCategoryVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 分类名称 */
    @Excel(name = "分类名称")
    @Length(min=1,max = 30,message = "分类名称不能超过30字")
    @NotNull(message="分类名称不能为空")
    private String categoryName;

    /** 描述 */
    @Excel(name = "描述")
    @Length(min=1,max = 200,message = "描述不能超过200字")
    @NotNull(message="描述不能为空")
    private String categoryDesc;

    /** 上级分类id */
    @Excel(name = "上级分类id")
    private Long parentId;

    /** 子菜单 */
    private List<GoodsCategory> children = new ArrayList<GoodsCategory>();

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setCategoryName(String categoryName)
    {
        this.categoryName = categoryName;
    }
    public String getCategoryName()
    {
        return categoryName;
    }


    public void setCategoryDesc(String categoryDesc)
    {
        this.categoryDesc = categoryDesc;
    }

    public String getCategoryDesc()
    {
        return categoryDesc;
    }
    public void setParentId(Long parentId)
    {
        this.parentId = parentId;
    }

    public Long getParentId()
    {
        return parentId;
    }

    public List<GoodsCategory> getChildren() {
        return children;
    }

    public void setChildren(List<GoodsCategory> children) {
        this.children = children;
    }

}
