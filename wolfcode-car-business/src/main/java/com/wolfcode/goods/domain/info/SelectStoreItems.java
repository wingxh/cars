package com.wolfcode.goods.domain.info;


import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;
import lombok.Data;

@Data
public class SelectStoreItems {
    /** $column.columnComment */
    private Long id;

    /** 物品名称 */
    @Excel(name = "物品名称")
    private String goodsName;

    /** 封面 */
    @Excel(name = "封面")
    private String goodsCover;

    /** 分类 */
    @Excel(name = "分类")
    private String categoryId;

    /** 分类名称 */
    @Excel(name = "分类名称")
    private String categoryName;

    /** 品牌 */
    @Excel(name = "品牌")
    private String brand;

    /** 数量 */
    @Excel(name = "数量")
    private Long amounts;

}
