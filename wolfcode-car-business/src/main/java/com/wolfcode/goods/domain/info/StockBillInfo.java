package com.wolfcode.goods.domain.info;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 出入库单据对象 stock_bill
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
@Data
public class StockBillInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 单据流水号 */
    private Long id;

    /** 类型, 0 入库, 1 出库 */
    @Excel(name = "类型, 0 入库, 1 出库")
    private Integer type;

    /** 仓库id */
    @Excel(name = "仓库id")
    private Long storeId;

    /** 出入库时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "出入库时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date busiDate;

    /** 0 正常, -1 作废 */
    @Excel(name = "0 正常, -1 作废")
    private Integer status;

    /** 操作时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date operateDate;

    /** 操作人id */
    @Excel(name = "操作人")
    private Long operatorId;

    /** 总数量 */
    private BigDecimal totalCount;

    /** 总金额 */
    private BigDecimal totalPrice;

    /** 仓库名 */
    private String storeName;

    /** 操作人 */
    private String operator;

}
