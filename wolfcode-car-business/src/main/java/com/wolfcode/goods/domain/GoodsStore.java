package com.wolfcode.goods.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;

/**
 * 物品库存对象 goods_store
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
@Data
public class GoodsStore extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 物品id */
    @Excel(name = "物品id")
    private Long goodsId;

    /** 仓库id */
    @Excel(name = "仓库id")
    private Long storeId;

    /** 库存数量 */
    @Excel(name = "库存数量")
    private Long amounts;

}
