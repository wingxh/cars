package com.wolfcode.goods.enums;

// 出入库状态
public enum BusStockStatusEnum {
    OUTPUT, // 出库
    INPUT // 入库
}
