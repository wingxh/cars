package com.wolfcode.goods.enums;

// 出入库状态
public enum BusBillStatusEnum {
    CANCEL, // 作废
    NORMAL // 正常
}
