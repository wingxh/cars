package com.wolfcode.goods.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wolfcode.goods.mapper.GoodsStoreMapper;
import com.wolfcode.goods.domain.GoodsStore;
import com.wolfcode.goods.service.IGoodsStoreService;

/**
 * 物品库存Service业务层处理
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
@Service
public class GoodsStoreServiceImpl implements IGoodsStoreService 
{
    @Autowired
    private GoodsStoreMapper goodsStoreMapper;

    /**
     * 查询物品库存
     * 
     * @param id 物品库存主键
     * @return 物品库存
     */
    @Override
    public GoodsStore selectGoodsStoreById(Long id)
    {
        return goodsStoreMapper.selectGoodsStoreById(id);
    }

    /**
     * 查询物品库存列表
     * 
     * @param goodsStore 物品库存
     * @return 物品库存
     */
    @Override
    public List<GoodsStore> selectGoodsStoreList(GoodsStore goodsStore)
    {
        return goodsStoreMapper.selectGoodsStoreList(goodsStore);
    }

    /**
     * 新增物品库存
     * 
     * @param goodsStore 物品库存
     * @return 结果
     */
    @Override
    public int insertGoodsStore(GoodsStore goodsStore)
    {
        return goodsStoreMapper.insertGoodsStore(goodsStore);
    }

    /**
     * 修改物品库存
     * 
     * @param goodsStore 物品库存
     * @return 结果
     */
    @Override
    public int updateGoodsStore(GoodsStore goodsStore)
    {
        return goodsStoreMapper.updateGoodsStore(goodsStore);
    }

    /**
     * 批量删除物品库存
     * 
     * @param ids 需要删除的物品库存主键
     * @return 结果
     */
    @Override
    public int deleteGoodsStoreByIds(Long[] ids)
    {
        return goodsStoreMapper.deleteGoodsStoreByIds(ids);
    }

    /**
     * 删除物品库存信息
     * 
     * @param id 物品库存主键
     * @return 结果
     */
    @Override
    public int deleteGoodsStoreById(Long id)
    {
        return goodsStoreMapper.deleteGoodsStoreById(id);
    }
}
