package com.wolfcode.goods.service.impl;

import java.util.List;

import com.wolfcode.goods.domain.GoodsStore;
import com.wolfcode.goods.domain.vo.StoreVo;
import org.springframework.beans.BeanUtils;

import com.wolfcode.common.utils.PageUtils;
import com.wolfcode.goods.domain.info.SelectStoreItems;
import com.wolfcode.goods.domain.vo.StoreListVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wolfcode.goods.mapper.StoreMapper;
import com.wolfcode.goods.domain.Store;
import com.wolfcode.goods.service.IStoreService;
import org.springframework.util.Assert;

/**
 * 仓库信息Service业务层处理
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
@Service
public class StoreServiceImpl implements IStoreService 
{
    @Autowired
    private StoreMapper storeMapper;

    /**
     * 查询仓库信息
     * 
     * @param id 仓库信息主键
     * @return 仓库信息
     */
    @Override
    public Store selectStoreById(Long id)
    {
        return storeMapper.selectStoreById(id);
    }

    /**
     * 查询仓库信息列表
     * 
     * @param store 仓库信息
     * @return 仓库信息
     */
    @Override
    public List<Store> selectStoreList(Store store)
    {
        return storeMapper.selectStoreList(store);
    }

    /**
     * 新增仓库信息
     * 
     * @param storeVo 仓库信息
     * @return 结果
     */
    @Override
    public int insertStore(StoreVo storeVo)
    {
        Assert.notNull(storeVo,"非法参数");
        Assert.hasText(storeVo.getStoreName(),"仓库名称必填或者不为空");
        Assert.state(storeVo.getStoreName().length() <= 50,"仓库名称不超过50个字");
        Assert.hasText(storeVo.getStoreAddress(),"仓库地址必填或者不为空");
        Assert.state(storeVo.getStoreAddress().length() <= 250,"仓库地址不超过250个字");

        Store store = new Store();
        BeanUtils.copyProperties(storeVo,store);
        store.setState(0);
        return storeMapper.insertStore(store);
    }

    /**
     * 修改仓库信息
     * 
     * @param storeVo 仓库信息
     * @return 结果
     */
    @Override
    public int updateStore(StoreVo storeVo)
    {
        Assert.notNull(storeVo,"非法参数");
        Assert.hasText(storeVo.getStoreName(),"仓库名称必填或者不为空");
        Assert.state(storeVo.getStoreName().length() <= 50,"仓库名称不超过50个字");
        Assert.hasText(storeVo.getStoreAddress(),"仓库地址必填或者不为空");
        Assert.state(storeVo.getStoreAddress().length() <= 250,"仓库地址不超过250个字");
        Store store = new Store();
        BeanUtils.copyProperties(storeVo,store);
        return storeMapper.updateStore(store);
    }

    /**
     * 批量删除仓库信息
     * 
     * @param ids 需要删除的仓库信息主键
     * @return 结果
     */
    @Override
    public int deleteStoreByIds(Long[] ids)
    {
        return storeMapper.deleteStoreByIds(ids);
    }

    /**
     * 删除仓库信息信息
     * 
     * @param id 仓库信息主键
     * @return 结果
     */
    @Override
    public void deleteStoreById(Long id)
    {
        Assert.notNull(id,"非法参数");
        // 根据仓库id判断仓库是否存有物品
        int result = storeMapper.selectGoodStoreByStoreId(id);
        Assert.state(result==0,"该仓库存有物品不能删除");

        storeMapper.deleteGoodStoreByStoreId(id);
        storeMapper.deleteStoreById(id);
    }

    @Override
    public List<Store> storeList() {
        return storeMapper.storeList();
    }

    @Override
    public List<SelectStoreItems> SelectStoreItems( StoreListVo storeListVo,Long id) {
        storeListVo.setStoreId(id);
//        PageUtils.startPage();
        List<SelectStoreItems> selectStoreItems = storeMapper.SelectStoreItems(storeListVo);
        return selectStoreItems;
    }

    @Override
    public List<Store> getStoreList() {

        return storeMapper.getStoreList();
    }
}
