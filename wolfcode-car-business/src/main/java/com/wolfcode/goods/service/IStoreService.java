package com.wolfcode.goods.service;

import java.util.List;
import com.wolfcode.goods.domain.Store;
import com.wolfcode.goods.domain.info.SelectStoreItems;
import com.wolfcode.goods.domain.vo.StoreListVo;
import com.wolfcode.goods.domain.vo.StoreVo;
import com.wolfcode.goods.domain.info.GoodsInfo;

/**
 * 仓库信息Service接口
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
public interface IStoreService 
{
    /**
     * 查询仓库信息
     * 
     * @param id 仓库信息主键
     * @return 仓库信息
     */
    public Store selectStoreById(Long id);

    /**
     * 查询仓库信息列表
     * 
     * @param store 仓库信息
     * @return 仓库信息集合
     */
    public List<Store> selectStoreList(Store store);

    /**
     * 新增仓库信息
     * 
     * @param storeVo 仓库信息
     * @return 结果
     */
    public int insertStore(StoreVo storeVo);

    /**
     * 修改仓库信息
     * 
     * @param storeVo 仓库信息
     * @return 结果
     */
    public int updateStore(StoreVo storeVo);

    /**
     * 批量删除仓库信息
     * 
     * @param ids 需要删除的仓库信息主键集合
     * @return 结果
     */
    public int deleteStoreByIds(Long[] ids);

    /**
     * 删除仓库信息
     * 
     * @param id 仓库信息主键
     * @return 结果
     */
    public void deleteStoreById(Long id);

    public List<Store> storeList();

    public List<SelectStoreItems>SelectStoreItems(StoreListVo storeListVo,Long id);

    List<Store> getStoreList();
}
