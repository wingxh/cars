package com.wolfcode.goods.service.impl;

import java.util.List;

import com.wolfcode.common.utils.FileUpLoadUtils;
import com.wolfcode.goods.domain.info.GoodsInfo;
import com.wolfcode.goods.domain.info.StockRecord;
import com.wolfcode.goods.domain.qo.GoodsQo;
import com.wolfcode.goods.domain.vo.GoodsVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wolfcode.goods.mapper.GoodsMapper;
import com.wolfcode.goods.domain.Goods;
import com.wolfcode.goods.service.IGoodsService;
import org.springframework.util.Assert;

/**
 * 物品信息Service业务层处理
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
@Service
public class GoodsServiceImpl implements IGoodsService 
{
    @Autowired
    private GoodsMapper goodsMapper;

    /**
     * 查询物品信息
     * 
     * @param id 物品信息主键
     * @return 物品信息
     */
    @Override
    public GoodsInfo selectGoodsById(Long id)
    {
        return goodsMapper.selectGoodsById(id);
    }

    @Override
    public int selectGoodsCountByCategoryIds(Long[] categoryIds) {
        return goodsMapper.selectGoodsCountByCategoryIds(categoryIds);
    }

    /**
     * 查询物品信息列表
     * 
     * @param goodsQo 物品信息,仓库,分类
     * @return 物品信息
     */
    @Override
    public List<GoodsInfo> selectGoodsList(GoodsQo goodsQo)
    {
        return goodsMapper.selectGoodsList(goodsQo);
    }

    /**
     * 新增物品信息
     * 
     * @param goodsVo 物品信息
     * @return 结果
     */
    @Override
    public int insertGoods(GoodsVo goodsVo) throws Exception {
        Assert.notNull(goodsVo,"非法参数");
        Assert.notNull(goodsVo.getGoodsCover(),"图片不能为空");
        String fileAddress = FileUpLoadUtils.getFileAddress(goodsVo.getGoodsCover());
        Goods goods = new Goods();
        BeanUtils.copyProperties(goodsVo,goods);
        goods.setGoodsCover(fileAddress);
        return goodsMapper.insertGoods(goods);
    }

    /**
     * 修改物品信息
     * 
     * @param goodsVo 物品信息
     * @return 结果
     */
    @Override
    public int updateGoods(GoodsVo goodsVo) throws Exception {
        Assert.notNull(goodsVo,"非法参数");
        Goods goods = new Goods();
        if (goodsVo.getGoodsCover() != null) {
            goods.setGoodsCover( FileUpLoadUtils.getFileAddress(goodsVo.getGoodsCover()));
        }
        BeanUtils.copyProperties(goodsVo,goods);
        return goodsMapper.updateGoods(goods);
    }

    /**
     * 批量删除物品信息
     * 
     * @param ids 需要删除的物品信息主键
     * @return 结果
     */
    @Override
    public int deleteGoodsByIds(Long[] ids)
    {
        return goodsMapper.deleteGoodsByIds(ids);
    }

    /**
     * 删除物品信息信息
     * 
     * @param id 物品信息主键
     * @return 结果
     */
    @Override
    public int deleteGoodsById(Long id)
    {
        return goodsMapper.deleteGoodsById(id);
    }

    /**
     * 通过物品id查询出入库明细
     * @param goodsId
     * @return
     */
    @Override
    public List<StockRecord> selectStockRecordsByGoodsId(Long goodsId) {

        return goodsMapper.selectStockRecordsByGoodsId(goodsId);
    }
}
