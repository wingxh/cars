package com.wolfcode.goods.service;

import java.util.List;

import com.wolfcode.common.core.domain.TreeSelect;
import com.wolfcode.common.core.domain.entity.SysDept;
import com.wolfcode.goods.domain.GoodsCategory;
import com.wolfcode.goods.domain.info.SelectCategoryInfo;
import com.wolfcode.goods.domain.vo.GoodsCategoryVo;

/**
 * 物品分类信息Service接口
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
public interface IGoodsCategoryService 
{
    /**
     * 查询物品分类信息
     * 
     * @param id 物品分类信息主键
     * @return 物品分类信息
     */
    public GoodsCategory selectGoodsCategoryById(Long id);

    /**
     * 查询物品分类信息列表
     * 
     * @param goodsCategory 物品分类信息
     * @return 物品分类信息集合
     */
    public List<GoodsCategory> selectGoodsCategoryList(GoodsCategory goodsCategory);

    /**
     * 获取商品分类下拉树列表
     * @param categories 商品分类列表
     * @return 下拉树列表
     */
    public List<TreeSelect> buildTreeSelect(List<GoodsCategory> categories);

    /**
     * 新增物品分类信息
     * 
     * @param categoryVo 物品分类信息
     * @return 结果
     */
    public void insertGoodsCategory(GoodsCategoryVo categoryVo);

    /**
     * 修改物品分类信息
     * 
     * @param categoryVo 物品分类信息
     * @return 结果
     */
    public void updateGoodsCategory(GoodsCategoryVo categoryVo);


    /**
     * 删除物品分类信息信息
     *
     * @param id 物品分类信息主键
     * @return 结果
     */
    public void deleteGoodsCategoryById(Long id);


    public List<SelectCategoryInfo> selectGoodsCategory();
}
