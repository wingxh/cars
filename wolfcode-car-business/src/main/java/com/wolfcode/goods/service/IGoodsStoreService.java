package com.wolfcode.goods.service;

import java.util.List;
import com.wolfcode.goods.domain.GoodsStore;

/**
 * 物品库存Service接口
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
public interface IGoodsStoreService 
{
    /**
     * 查询物品库存
     * 
     * @param id 物品库存主键
     * @return 物品库存
     */
    public GoodsStore selectGoodsStoreById(Long id);

    /**
     * 查询物品库存列表
     * 
     * @param goodsStore 物品库存
     * @return 物品库存集合
     */
    public List<GoodsStore> selectGoodsStoreList(GoodsStore goodsStore);

    /**
     * 新增物品库存
     * 
     * @param goodsStore 物品库存
     * @return 结果
     */
    public int insertGoodsStore(GoodsStore goodsStore);

    /**
     * 修改物品库存
     * 
     * @param goodsStore 物品库存
     * @return 结果
     */
    public int updateGoodsStore(GoodsStore goodsStore);

    /**
     * 批量删除物品库存
     * 
     * @param ids 需要删除的物品库存主键集合
     * @return 结果
     */
    public int deleteGoodsStoreByIds(Long[] ids);

    /**
     * 删除物品库存信息
     * 
     * @param id 物品库存主键
     * @return 结果
     */
    public int deleteGoodsStoreById(Long id);
}
