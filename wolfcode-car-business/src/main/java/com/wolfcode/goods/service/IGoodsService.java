package com.wolfcode.goods.service;

import java.util.List;
import com.wolfcode.goods.domain.Goods;
import com.wolfcode.goods.domain.info.GoodsInfo;
import com.wolfcode.goods.domain.info.StockRecord;
import com.wolfcode.goods.domain.qo.GoodsQo;
import com.wolfcode.goods.domain.vo.GoodsVo;

/**
 * 物品信息Service接口
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
public interface IGoodsService 
{
    /**
     * 查询物品信息
     * 
     * @param id 物品信息主键
     * @return 物品信息
     */
    public GoodsInfo selectGoodsById(Long id);

    /**
     * 查询物品数量
     * @param categoryIds 类别
     * @return 数量
     */
    public int selectGoodsCountByCategoryIds(Long[] categoryIds);

    /**
     * 查询物品信息列表
     * 
     * @param goodsQo 物品信息,仓库,分类
     * @return 物品信息集合
     */
    public List<GoodsInfo> selectGoodsList(GoodsQo goodsQo);

    /**
     * 新增物品信息
     * 
     * @param goodsVo 物品信息
     * @return 结果
     */
    public int insertGoods(GoodsVo goodsVo) throws Exception;

    /**
     * 修改物品信息
     * 
     * @param goodsVo 物品信息
     * @return 结果
     */
    public int updateGoods(GoodsVo goodsVo) throws Exception;

    /**
     * 批量删除物品信息
     * 
     * @param ids 需要删除的物品信息主键集合
     * @return 结果
     */
    public int deleteGoodsByIds(Long[] ids);

    /**
     * 删除物品信息信息
     * 
     * @param id 物品信息主键
     * @return 结果
     */
    public int deleteGoodsById(Long id);

    /**
     * 通过物品id查询出入库明细
     * @param goodsId
     * @return
     */
    List<StockRecord> selectStockRecordsByGoodsId(Long goodsId);
}
