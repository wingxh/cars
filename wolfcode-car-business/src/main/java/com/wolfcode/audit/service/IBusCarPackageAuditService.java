package com.wolfcode.audit.service;

import java.io.InputStream;
import java.util.List;
import com.wolfcode.audit.domain.BusCarPackageAudit;
import com.wolfcode.audit.domain.info.HistoricCommentInfo;
import com.wolfcode.audit.domain.vo.BusCarPackageAuditVo;

/**
 * 套餐审核Service接口
 * 
 * @author wolfcode
 * @date 2022-11-28
 */
public interface IBusCarPackageAuditService 
{
    /**
     * 查询套餐审核
     * 
     * @param id 套餐审核主键
     * @return 套餐审核
     */
    public BusCarPackageAudit selectBusCarPackageAuditById(Long id);

    /**
     * 查询套餐审核列表
     * 
     * @param busCarPackageAudit 套餐审核
     * @return 套餐审核集合
     */
    public List<BusCarPackageAudit> selectBusCarPackageAuditList(BusCarPackageAudit busCarPackageAudit);

    /**
     * 新增套餐审核
     * 
     * @param busCarPackageAudit 套餐审核
     * @return 结果
     */
    public int insertBusCarPackageAudit(BusCarPackageAudit busCarPackageAudit);

    /**
     * 修改套餐审核
     * 
     * @param busCarPackageAudit 套餐审核
     * @return 结果
     */
    public int updateBusCarPackageAudit(BusCarPackageAudit busCarPackageAudit);

    /**
     * 批量删除套餐审核
     * 
     * @param ids 需要删除的套餐审核主键集合
     * @return 结果
     */
    public int deleteBusCarPackageAuditByIds(Long[] ids);

    /**
     * 删除套餐审核信息
     * 
     * @param id 套餐审核主键
     * @return 结果
     */
    public int deleteBusCarPackageAuditById(Long id);

    List<BusCarPackageAudit> todoQuery(BusCarPackageAuditVo busCarPackageAuditVo);

    void audit(BusCarPackageAuditVo auditVo);

    List<HistoricCommentInfo> listHistory(String instanceId);

    InputStream getProcessInputStream(Long id);

    List<BusCarPackageAudit> doneQuery(BusCarPackageAuditVo auditVo);
}
