package com.wolfcode.audit.enums;

public enum  PackageAuditEnum {
//    状态【审核中0/审核拒绝1/审核通过2/审核撤销3】
    // 审核中
    AUDITING,
    REJECT,
    PASS,
    CANCEL
}
