package com.wolfcode.audit.domain;

import java.math.BigDecimal;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;

/**
 * 套餐审核对象 bus_car_package_audit
 * 
 * @author wolfcode
 * @date 2022-11-28
 */
@Data
public class BusCarPackageAudit extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 服务单项id */
    @Excel(name = "服务单项id")
    private Long serviceItemId;

    /** 服务单项名称 */
    @Excel(name = "服务单项名称")
    private String serviceItemName;

    /** 服务单项备注 */
    @Excel(name = "服务单项备注")
    private String serviceItemInfo;

    /** 服务单项审核价格 */
    @Excel(name = "服务单项审核价格")
    private BigDecimal serviceItemPrice;

    /** 流程实例id */
    @Excel(name = "流程实例id")
    private String instanceId;

    /** 创建者 */
    @Excel(name = "创建者")
    private String creatorId;

    /** 备注 */
    @Excel(name = "备注")
    private String info;

    /** 状态【进行中0/审核拒绝1/审核通过2/审核撤销3】 */
    @Excel(name = "状态【进行中0/审核拒绝1/审核通过2/审核撤销3】")
    private Integer status;

}
