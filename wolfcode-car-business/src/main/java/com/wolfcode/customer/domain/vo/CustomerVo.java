package com.wolfcode.customer.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

/**
 * 客户信息对象 customer
 * 
 * @author wolfcode
 * @date 2022-12-09
 */
@Data
public class CustomerVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 客户名称 */
    @Excel(name = "企业名称")
    private String customerName;

    /** 法定代表人 */
    @Excel(name = "法定代表人")
    private String legalLeader;

    /** 成立时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "成立时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date registerDate;

    /** 经营状态, 0 开业、1 注销、2 破产 */
    @Excel(name = "经营状态, 0 开业、1 注销、2 破产")
    private Integer openState;

    /** 所属省份 */
    @Excel(name = "所属省份")
    private String province;

    /** 注册资本,(万元) */
    @Excel(name = "注册资本,(万元)")
    private BigDecimal regCapital;

    private void setRegCapital(BigDecimal regCapital){
        //设置精度
        regCapital.setScale(2, RoundingMode.DOWN);
        this.regCapital = regCapital;
    }
    /** 所属行业 */
    @Excel(name = "所属行业")
    private String industry;

    /** 经营范围 */
    @Excel(name = "经营范围")
    private String scope;

    /** 注册地址 */
    @Excel(name = "注册地址")
    private String regAddr;


}
