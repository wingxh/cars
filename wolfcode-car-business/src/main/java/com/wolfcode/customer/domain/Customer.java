package com.wolfcode.customer.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;

/**
 * 客户信息对象 customer
 *
 * @author wolfcode
 * @date 2022-12-09
 */
@Data
public class Customer extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 客户名称 */
    private String customerName;

    /** 法定代表人 */
    private String legalLeader;

    /** 成立时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date registerDate;

    /** 经营状态, 0 开业、1 注销、2 破产 */
    private Integer openState;

    /** 所属省份 */
    private String province;

    /** 注册资本,(万元) */
    private BigDecimal regCapital;

    /** 所属行业 */
    private String industry;

    /** 经营范围 */
    private String scope;

    /** 注册地址 */
    private String regAddr;

    /** 录入时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date inputTime;

    /** 录入人id */
    private Long inputUser;

    /** 录入人名称 */
    private String username;
}
