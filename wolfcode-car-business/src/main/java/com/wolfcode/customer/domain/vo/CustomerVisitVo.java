package com.wolfcode.customer.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 拜访信息对象 customer_visit
 * 
 * @author wolfcode
 * @date 2022-12-09
 */
@Data
public class CustomerVisitVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一id */
    private Long id;

    /** 客户id，关联到企业客户表 */
    private Long customerId;

    /** 联系人id，关联到客户联系人表 */
    private Long linkmanId;

    /** 拜访方式, 1 上门走访, 2 电话拜访 */
    private Integer visitType;

    /** 拜访原因 */
    private String visitReason;

    /** 交流内容 */
    private String content;

    /** 拜访时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date visitDate;
}
