package com.wolfcode.customer.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;

/**
 * 拜访信息对象 customer_visit
 * 
 * @author wolfcode
 * @date 2022-12-09
 */
@Data
public class CustomerVisit extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一id */
    private Long id;

    /** 客户id，关联到企业客户表 */
    @Excel(name = "客户id，关联到企业客户表")
    private Long customerId;

    /** 联系人id，关联到客户联系人表 */
    @Excel(name = "联系人id，关联到客户联系人表")
    private Long linkmanId;

    /** 拜访方式, 1 上门走访, 2 电话拜访 */
    @Excel(name = "拜访方式, 0 上门走访, 1 电话拜访")
    private Integer visitType;

    /** 拜访原因 */
    @Excel(name = "拜访原因")
    private String visitReason;

    /** 交流内容 */
    @Excel(name = "交流内容")
    private String content;

    /** 拜访时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "拜访时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date visitDate;

    /** 录入人 */
    @Excel(name = "录入人")
    private Long inputUser;

    /** 录入时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "录入时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date inputTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCustomerId(Long customerId) 
    {
        this.customerId = customerId;
    }

    public Long getCustomerId() 
    {
        return customerId;
    }
    public void setLinkmanId(Long linkmanId) 
    {
        this.linkmanId = linkmanId;
    }

    public Long getLinkmanId() 
    {
        return linkmanId;
    }
    public void setVisitType(Integer visitType) 
    {
        this.visitType = visitType;
    }

    public Integer getVisitType() 
    {
        return visitType;
    }
    public void setVisitReason(String visitReason) 
    {
        this.visitReason = visitReason;
    }

    public String getVisitReason() 
    {
        return visitReason;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setVisitDate(Date visitDate) 
    {
        this.visitDate = visitDate;
    }

    public Date getVisitDate() 
    {
        return visitDate;
    }
    public void setInputUser(Long inputUser) 
    {
        this.inputUser = inputUser;
    }

    public Long getInputUser() 
    {
        return inputUser;
    }
    public void setInputTime(Date inputTime) 
    {
        this.inputTime = inputTime;
    }

    public Date getInputTime() 
    {
        return inputTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("customerId", getCustomerId())
            .append("linkmanId", getLinkmanId())
            .append("visitType", getVisitType())
            .append("visitReason", getVisitReason())
            .append("content", getContent())
            .append("visitDate", getVisitDate())
            .append("inputUser", getInputUser())
            .append("inputTime", getInputTime())
            .toString();
    }
}
