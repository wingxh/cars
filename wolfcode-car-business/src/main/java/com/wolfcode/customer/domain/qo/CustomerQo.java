package com.wolfcode.customer.domain.qo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wolfcode.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;
@Data
//模糊查询
public class CustomerQo extends BaseEntity {
        /** 关键字 */
        private String keyword;

        /** 经营状态, 0 开业、1 注销、2 破产 */
        private Integer openState;

        /** 所属省份 */
        private String province;

        /** 录入时间 */
        @JsonFormat(pattern = "yyyy-MM-dd")
        private Date inputTime;

}
