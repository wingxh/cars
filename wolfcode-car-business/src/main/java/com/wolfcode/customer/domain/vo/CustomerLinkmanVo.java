package com.wolfcode.customer.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 客户联系人对象 customer_linkman
 * 
 * @author wolfcode
 * @date 2022-12-09
 */
@Data
public class CustomerLinkmanVo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /**  */
    private String keyword;

    /** 客户id，关联到企业客户表 */
    private String customerId;

    /** 联系人名字 */
    private String linkman;

    /** 性别 1 男 0 女 */
    private Integer gender;

    /** 年龄 */
    private Integer age;

    /** 联系人电话 */
    private String phone;

    /** 职位 */
    private String position;

    /** 任职状态 */
    private Integer positionState;

    /** 部门 */
    private String department;
}
