package com.wolfcode.customer.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;

/**
 * 客户联系人对象 customer_linkman
 * 
 * @author wolfcode
 * @date 2022-12-09
 */
@Data
public class CustomerLinkman extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long id;

    /** 客户id，关联到企业客户表 */
    @Excel(name = "客户id，关联到企业客户表")
    private String customerId;

    /** 联系人名字 */
    @Excel(name = "联系人名字")
    private String linkman;

    /** 性别 1 男 0 女 */
    @Excel(name = "性别 1 男 0 女")
    private Integer gender;

    /** 年龄 */
    @Excel(name = "年龄")
    private Integer age;

    /** 联系人电话 */
    @Excel(name = "联系人电话")
    private String phone;

    /** 职位 */
    @Excel(name = "职位")
    private String position;

    /** 任职状态 */
    @Excel(name = "任职状态")
    private Integer positionState;

    /** 部门 */
    @Excel(name = "部门")
    private String department;

    /** 录入人 */
    @Excel(name = "录入人")
    private Long inputUser;

    /** 录入时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "录入时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date inputTime;
}
