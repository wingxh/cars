package com.wolfcode.customer.domain.info;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 拜访信息对象 customer_visit
 * 
 * @author wolfcode
 * @date 2022-12-09
 */
@Data
public class CustomerVisitInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一id */
    private Long id;

    /** 客户id，关联到企业客户表 */
    @Excel(name = "客户，关联到企业客户表")
    private String customerName;

    /** 联系人id，关联到客户联系人表 */
    @Excel(name = "联系人，关联到客户联系人表")
    private String linkmanName;

    /** 拜访方式, 1 上门走访, 2 电话拜访 */
    @Excel(name = "拜访方式, 0 上门走访, 1 电话拜访")
    private Integer visitType;

    /** 拜访原因 */
    @Excel(name = "拜访原因")
    private String visitReason;

    /** 交流内容 */
    @Excel(name = "交流内容")
    private String content;

    /** 拜访时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "拜访时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date visitDate;

    /** 录入人 */
    @Excel(name = "录入人")
    private String inputUser;

    /** 录入时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "录入时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date inputTime;
}
