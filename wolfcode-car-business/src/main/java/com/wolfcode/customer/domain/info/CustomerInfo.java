package com.wolfcode.customer.domain.info;

import com.wolfcode.common.annotation.Excel;
import lombok.Data;

@Data
public class CustomerInfo {
    private Long customerId;
    /** 客户名称 */
    private String customerName;
}
