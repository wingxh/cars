package com.wolfcode.customer.mapper;

import java.util.List;
import com.wolfcode.customer.domain.Customer;
import com.wolfcode.customer.domain.info.CustomerInfo;
import com.wolfcode.customer.domain.qo.CustomerQo;
import com.wolfcode.customer.domain.vo.CustomerVo;

/**
 * 客户信息Mapper接口
 *
 * @author wolfcode
 * @date 2022-12-09
 */
public interface CustomerMapper
{
    /**
     * 查询客户信息
     *
     * @param id 客户信息主键
     * @return 客户信息
     */
    public Customer selectCustomerById(Long id);

    /**
     * 查询客户信息列表
     *
     * @param customerQo 客户信息
     * @return 客户信息集合
     */
    public List<Customer> selectCustomerList(CustomerQo customerQo);

    /**
     * 新增客户信息
     *
     * @param customer 客户信息
     * @return 结果
     */
    public int insertCustomer(Customer customer);

    /**
     * 修改客户信息
     * @param customerVo 客户信息
     * @return 结果
     */
    public int updateCustomer(CustomerVo customerVo);

    /**
     * 删除客户信息
     *
     * @param id 客户信息主键
     * @return 结果
     */
    public int deleteCustomerById(Long id);

    /**
     * 批量删除客户信息
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCustomerByIds(Long[] ids);

    /**
     * 通过数据库中的名字校验名字
     */
    List<String> selectCustomerName(String customerName);

    /**
     * 查询客户信息列表
     *
     * @return 结果
     */
    public List<CustomerInfo> getCustomerInfo();
}
