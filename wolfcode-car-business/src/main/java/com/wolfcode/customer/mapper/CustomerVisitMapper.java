package com.wolfcode.customer.mapper;

import java.util.List;

import com.wolfcode.customer.domain.Customer;
import com.wolfcode.customer.domain.CustomerVisit;
import com.wolfcode.customer.domain.info.CustomerVisitInfo;

/**
 * 拜访信息Mapper接口
 * 
 * @author wolfcode
 * @date 2022-12-09
 */
public interface CustomerVisitMapper 
{
    /**
     * 查询拜访信息
     * 
     * @param id 拜访信息主键
     * @return 拜访信息
     */
    public CustomerVisitInfo selectCustomerVisitById(Long id);

    /**
     * 查询拜访信息列表
     * 
     * @param customerVisit 拜访信息
     * @return 拜访信息集合
     */
    public List<CustomerVisitInfo> selectCustomerVisitList(CustomerVisit customerVisit);

    /**
     * 新增拜访信息
     * 
     * @param customerVisit 拜访信息
     * @return 结果
     */
    public int insertCustomerVisit(CustomerVisit customerVisit);

    /**
     * 修改拜访信息
     * 
     * @param customerVisit 拜访信息
     * @return 结果
     */
    public int updateCustomerVisit(CustomerVisit customerVisit);

    /**
     * 删除拜访信息
     * 
     * @param id 拜访信息主键
     * @return 结果
     */
    public int deleteCustomerVisitById(Long id);

    /**
     * 批量删除拜访信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCustomerVisitByIds(Long[] ids);

    public List<Customer> getCustomers();
}
