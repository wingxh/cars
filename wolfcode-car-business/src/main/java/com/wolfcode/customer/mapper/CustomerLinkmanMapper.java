package com.wolfcode.customer.mapper;

import java.util.List;
import com.wolfcode.customer.domain.CustomerLinkman;
import com.wolfcode.customer.domain.info.CustomerLinkmanInfo;
import com.wolfcode.customer.domain.vo.CustomerLinkmanVo;

/**
 * 客户联系人Mapper接口
 * 
 * @author wolfcode
 * @date 2022-12-09
 */
public interface CustomerLinkmanMapper 
{
    /**
     * 查询客户联系人
     * 
     * @param id 客户联系人主键
     * @return 客户联系人
     */
    public CustomerLinkman selectCustomerLinkmanById(Long id);

    /**
     * 查询客户联系人列表
     * 
     * @param customerLinkmanVo 客户联系人
     * @return 客户联系人集合
     */
    public List<CustomerLinkmanInfo> selectCustomerLinkmanList(CustomerLinkmanVo customerLinkmanVo);

    /**
     * 新增客户联系人
     * 
     * @param customerLinkman 客户联系人
     * @return 结果
     */
    public int insertCustomerLinkman(CustomerLinkman customerLinkman);

    /**
     * 修改客户联系人
     * 
     * @param customerLinkman 客户联系人
     * @return 结果
     */
    public int updateCustomerLinkman(CustomerLinkman customerLinkman);

    /**
     * 删除客户联系人
     * 
     * @param id 客户联系人主键
     * @return 结果
     */
    public int deleteCustomerLinkmanById(Long id);

    /**
     * 批量删除客户联系人
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCustomerLinkmanByIds(Long[] ids);

    public List<CustomerLinkman> search(Long customerId);
}
