package com.wolfcode.customer.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.wolfcode.customer.domain.info.CustomerVisitInfo;
import com.wolfcode.customer.domain.vo.CustomerVisitVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wolfcode.common.annotation.Log;
import com.wolfcode.common.core.controller.BaseController;
import com.wolfcode.common.core.domain.AjaxResult;
import com.wolfcode.common.enums.BusinessType;
import com.wolfcode.customer.domain.CustomerVisit;
import com.wolfcode.customer.service.ICustomerVisitService;
import com.wolfcode.common.utils.poi.ExcelUtil;
import com.wolfcode.common.core.page.TableDataInfo;

/**
 * 拜访信息Controller
 * 
 * @author wolfcode
 * @date 2022-12-09
 */
@RestController
@RequestMapping("/customer/visit")
public class CustomerVisitController extends BaseController
{
    @Autowired
    private ICustomerVisitService customerVisitService;

    /**
     * 查询拜访信息列表
     */
    @PreAuthorize("@ss.hasPermi('customer:visit:list')")
    @GetMapping("/list")
    public TableDataInfo list(CustomerVisit customerVisit)
    {
        startPage();
        List<CustomerVisitInfo> list = customerVisitService.selectCustomerVisitList(customerVisit);
        return getDataTable(list);
    }

    /**
     * 导出拜访信息列表
     */
    @PreAuthorize("@ss.hasPermi('customer:visit:export')")
    @Log(title = "拜访信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CustomerVisit customerVisit)
    {
        List<CustomerVisitInfo> list = customerVisitService.selectCustomerVisitList(customerVisit);
        ExcelUtil<CustomerVisitInfo> util = new ExcelUtil<CustomerVisitInfo>(CustomerVisitInfo.class);
        util.exportExcel(response, list, "拜访信息数据");
    }

    /**
     * 获取拜访信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('customer:visit:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(customerVisitService.selectCustomerVisitById(id));
    }

    /**
     * 新增拜访信息
     */
    @PreAuthorize("@ss.hasPermi('customer:visit:add')")
    @Log(title = "拜访信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CustomerVisitVo customerVisitVo)
    {
        return toAjax(customerVisitService.insertCustomerVisit(customerVisitVo));
    }

    /**
     * 获取所有客户
     */
    @PreAuthorize("@ss.hasPermi('customer:visit:getCustomers')")
    @GetMapping("/getCustomers")
    public AjaxResult getCustomers()
    {
        return AjaxResult.success(customerVisitService.getCustomers());
    }
}
