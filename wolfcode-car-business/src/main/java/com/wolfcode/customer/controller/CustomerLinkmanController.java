package com.wolfcode.customer.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.wolfcode.customer.domain.info.CustomerLinkmanInfo;
import com.wolfcode.customer.domain.vo.CustomerLinkmanVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wolfcode.common.annotation.Log;
import com.wolfcode.common.core.controller.BaseController;
import com.wolfcode.common.core.domain.AjaxResult;
import com.wolfcode.common.enums.BusinessType;
import com.wolfcode.customer.domain.CustomerLinkman;
import com.wolfcode.customer.service.ICustomerLinkmanService;
import com.wolfcode.common.utils.poi.ExcelUtil;
import com.wolfcode.common.core.page.TableDataInfo;

/**
 * 客户联系人Controller
 * 
 * @author wolfcode
 * @date 2022-12-09
 */
@RestController
@RequestMapping("/customer/linkman")
public class CustomerLinkmanController extends BaseController
{
    @Autowired
    private ICustomerLinkmanService customerLinkmanService;

    /**
     * 查询客户联系人列表
     */
    @PreAuthorize("@ss.hasPermi('customer:linkman:list')")
    @GetMapping("/list")
    public TableDataInfo list(CustomerLinkmanVo customerLinkmanVo)
    {
        startPage();
        List<CustomerLinkmanInfo> list = customerLinkmanService.selectCustomerLinkmanList(customerLinkmanVo);
        return getDataTable(list);
    }

    /**
     * 导出客户联系人列表
     */
    /*@PreAuthorize("@ss.hasPermi('customer:linkman:export')")
    @Log(title = "客户联系人", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CustomerLinkmanVo customerLinkmanVo)
    {
        List<CustomerLinkman> list = customerLinkmanService.selectCustomerLinkmanList(customerLinkmanVo);
        ExcelUtil<CustomerLinkman> util = new ExcelUtil<CustomerLinkman>(CustomerLinkman.class);
        util.exportExcel(response, list, "客户联系人数据");
    }*/

    /**
     * 获取客户联系人详细信息
     */
    @PreAuthorize("@ss.hasPermi('customer:linkman:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(customerLinkmanService.selectCustomerLinkmanById(id));
    }

    /**
     * 新增客户联系人
     */
    @PreAuthorize("@ss.hasPermi('customer:linkman:add')")
    @Log(title = "客户联系人", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CustomerLinkman customerLinkman)
    {
        return toAjax(customerLinkmanService.insertCustomerLinkman(customerLinkman));
    }

    /**
     * 修改客户联系人
     */
    @PreAuthorize("@ss.hasPermi('customer:linkman:edit')")
    @Log(title = "客户联系人", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CustomerLinkman customerLinkman)
    {
        return toAjax(customerLinkmanService.updateCustomerLinkman(customerLinkman));
    }

    /**
     * 删除客户联系人
     */
    @PreAuthorize("@ss.hasPermi('customer:linkman:remove')")
    @Log(title = "客户联系人", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(customerLinkmanService.deleteCustomerLinkmanByIds(ids));
    }

    /**
     * 获取客户联系人
     */
    @PreAuthorize("@ss.hasPermi('customer:linkman:search')")
    @GetMapping("search/{id}")
    public AjaxResult search(@PathVariable Long id)
    {
        return AjaxResult.success(customerLinkmanService.search(id));
    }

}
