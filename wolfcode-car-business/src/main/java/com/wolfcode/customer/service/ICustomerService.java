package com.wolfcode.customer.service;

import java.util.List;
import com.wolfcode.customer.domain.Customer;
import com.wolfcode.customer.domain.qo.CustomerQo;
import com.wolfcode.customer.domain.vo.CustomerVo;
import com.wolfcode.customer.domain.info.CustomerInfo;

/**
 * 客户信息Service接口
 *
 * @author wolfcode
 * @date 2022-12-09
 */
public interface ICustomerService
{
    /**
     * 查询客户信息
     *
     * @param id 客户信息主键
     * @return 客户信息
     */
    public Customer selectCustomerById(Long id);

    /**
     * 查询客户信息列表
     *
     * @param customerQo 客户信息
     * @return 客户信息集合
     */
    public List<Customer> selectCustomerList(CustomerQo customerQo);

    /**
     * 新增客户信息
     * 
     * @param customerVo 客户信息
     * @return 结果
     */
    public int insertCustomer(CustomerVo customerVo);

    /**
     * 修改客户信息
     *
     * @param customerVo 客户信息
     * @return 结果
     */
    public int updateCustomer(CustomerVo customerVo);

    /**
     * 批量删除客户信息
     *
     * @param ids 需要删除的客户信息主键集合
     * @return 结果
     */
    public int deleteCustomerByIds(Long[] ids);

    /**
     * 删除客户信息信息
     *
     * @param id 客户信息主键
     * @return 结果
     */
    public int deleteCustomerById(Long id);

    /**
     * 查询客户信息列表
     *
     * @return 结果
     */
    public List<CustomerInfo> getCustomerInfo();
}
