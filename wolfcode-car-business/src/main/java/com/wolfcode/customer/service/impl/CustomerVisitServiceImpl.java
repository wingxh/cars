package com.wolfcode.customer.service.impl;

import java.util.List;

import com.wolfcode.common.utils.DateUtils;
import com.wolfcode.common.utils.SecurityUtils;
import com.wolfcode.customer.domain.Customer;
import com.wolfcode.customer.domain.info.CustomerVisitInfo;
import com.wolfcode.customer.domain.vo.CustomerVisitVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wolfcode.customer.mapper.CustomerVisitMapper;
import com.wolfcode.customer.domain.CustomerVisit;
import com.wolfcode.customer.service.ICustomerVisitService;
import org.springframework.util.Assert;

/**
 * 拜访信息Service业务层处理
 * 
 * @author wolfcode
 * @date 2022-12-09
 */
@Service
public class CustomerVisitServiceImpl implements ICustomerVisitService 
{
    @Autowired
    private CustomerVisitMapper customerVisitMapper;

    /**
     * 查询拜访信息
     * 
     * @param id 拜访信息主键
     * @return 拜访信息
     */
    @Override
    public CustomerVisitInfo selectCustomerVisitById(Long id)
    {
        return customerVisitMapper.selectCustomerVisitById(id);
    }

    /**
     * 查询拜访信息列表
     * 
     * @param customerVisit 拜访信息
     * @return 拜访信息
     */
    @Override
    public List<CustomerVisitInfo> selectCustomerVisitList(CustomerVisit customerVisit)
    {
        Assert.notNull(customerVisit,"非法参数");
        return customerVisitMapper.selectCustomerVisitList(customerVisit);
    }

    /**
     * 新增拜访信息
     * 
     * @param customerVisitVo 拜访信息
     * @return 结果
     */
    @Override
    public int insertCustomerVisit(CustomerVisitVo customerVisitVo)
    {
        Assert.notNull(customerVisitVo,"非法参数");
        Assert.state(customerVisitVo.getVisitDate().compareTo(DateUtils.getNowDate()) != 1,"拜访时间不能超过当前时间");
        CustomerVisit customerVisit = new CustomerVisit();
        customerVisit.setInputTime(DateUtils.getNowDate());
        customerVisit.setInputUser(SecurityUtils.getUserId());
        BeanUtils.copyProperties(customerVisitVo,customerVisit);
        return customerVisitMapper.insertCustomerVisit(customerVisit);
    }

    @Override
    public List<Customer> getCustomers() {
        return customerVisitMapper.getCustomers();
    }
}
