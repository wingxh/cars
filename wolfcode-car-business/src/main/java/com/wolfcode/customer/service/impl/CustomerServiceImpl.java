package com.wolfcode.customer.service.impl;

import java.math.BigDecimal;
import java.util.List;

import com.wolfcode.common.utils.DateUtils;
import com.wolfcode.common.utils.SecurityUtils;
import com.wolfcode.customer.domain.vo.CustomerVo;
import org.springframework.beans.BeanUtils;

import com.wolfcode.customer.domain.qo.CustomerQo;

import com.wolfcode.customer.domain.info.CustomerInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wolfcode.customer.mapper.CustomerMapper;
import com.wolfcode.customer.domain.Customer;
import com.wolfcode.customer.service.ICustomerService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * 客户信息Service业务层处理
 *
 * @author wolfcode
 * @date 2022-12-09
 */
@Service
public class CustomerServiceImpl implements ICustomerService
{
    @Autowired
    private CustomerMapper customerMapper;

    /**
     * 查询客户信息
     *
     * @param id 客户信息主键
     * @return 客户信息
     */
    @Override
    public Customer selectCustomerById(Long id)
    {
        return customerMapper.selectCustomerById(id);
    }

    /**
     * 查询客户信息列表
     *
     * @param customerQo 客户信息
     * @return 客户信息
     */
    @Override
    public List<Customer> selectCustomerList(CustomerQo customerQo)
    {
        return customerMapper.selectCustomerList(customerQo);
    }

    /**
     * 新增客户信息
     * 
     * @param customerVo 客户信息
     * @return 结果
     */
    @Override
    public int insertCustomer(CustomerVo customerVo)
    {
        //校验参数
        Assert.notNull(customerVo,"非法参数");
        Assert.hasLength(customerVo.getCustomerName(),"企业名称必填");
        Assert.state(customerVo.getCustomerName().length() <= 100,"企业名字在100字以内");
        List<String> list = customerMapper.selectCustomerName(customerVo.getCustomerName());
        Assert.state(list.isEmpty(),"企业名称不能重复");
        Assert.hasLength(customerVo.getLegalLeader(),"法定代表人不能为空");
        Assert.state(customerVo.getLegalLeader().length() <= 30,"法定代表人的名字不能大于30字");
        Assert.state(customerVo.getRegCapital()
                .compareTo(new BigDecimal("0")) >= 0 ,"注册金额不能是负数");
        Assert.hasLength(customerVo.getIndustry(),"所属行业不能为空");
        Assert.state(customerVo.getIndustry().length() <= 30,"所属行业的字数不能大于30");
        Assert.state(customerVo.getScope().length() < 500 ,"经营范围在500字以内");
        Assert.hasLength(customerVo.getScope(),"经营范围不能为空");
        Assert.hasLength(customerVo.getRegAddr(),"注册地址不能为空");
        Assert.state(customerVo.getRegAddr().length() < 500,"注册地址的字数在500字以内");
        //设置录入人,落库
        Customer customer1 = new Customer();
        BeanUtils.copyProperties(customerVo,customer1);
        customer1.setInputUser(SecurityUtils.getUserId());
        customer1.setInputTime(DateUtils.getNowDate());
        return customerMapper.insertCustomer(customer1);
    }

    /**
     * 修改客户信息
     * @param customerVo 客户信息
     * @return 结果
     */
    @Override
    @Transactional
    public int updateCustomer(CustomerVo customerVo)
    {
        //校验参数
        Assert.notNull(customerVo,"非法参数");
        Assert.hasLength(customerVo.getCustomerName(),"企业名称必填");
        Assert.state(customerVo.getCustomerName().length() <= 100,"企业名字在100字以内");
        Assert.hasLength(customerVo.getLegalLeader(),"法定代表人不能为空");
        Assert.state(customerVo.getLegalLeader().length() <= 30,"法定代表人的名字不能大于30字");
        Assert.state(customerVo.getRegCapital()
                .compareTo(new BigDecimal("0")) >= 0 ,"注册金额不能是负数");
        Assert.hasLength(customerVo.getIndustry(),"所属行业不能为空");
        Assert.state(customerVo.getIndustry().length() <= 30,"所属行业的字数不能大于30");
        Assert.state(customerVo.getScope().length() < 500 ,"经营范围在500字以内");
        Assert.hasLength(customerVo.getScope(),"经营范围不能为空");
        Assert.hasLength(customerVo.getRegAddr(),"注册地址不能为空");
        Assert.state(customerVo.getRegAddr().length() < 500,"注册地址的字数在500字以内");
        //利用事务机制
        customerMapper.updateCustomer(customerVo);
        List<String> list = customerMapper.selectCustomerName(customerVo.getCustomerName());
        Assert.state(list.size() >= 1,"企业名称不能重复");
        return 1;
    }

    /**
     * 批量删除客户信息
     *
     * @param ids 需要删除的客户信息主键
     * @return 结果
     */
    @Override
    public int deleteCustomerByIds(Long[] ids)
    {
        return customerMapper.deleteCustomerByIds(ids);
    }

    /**
     * 删除客户信息信息
     *
     * @param id 客户信息主键
     * @return 结果
     */
    @Override
    public int deleteCustomerById(Long id)
    {
        return customerMapper.deleteCustomerById(id);
    }


    /**
     * 查询客户信息列表
     *
     * @return 结果
     */
    @Override
    public List<CustomerInfo> getCustomerInfo() {
        return customerMapper.getCustomerInfo();
    }
}
