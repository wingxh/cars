package com.wolfcode.customer.service.impl;

import java.util.List;

import com.wolfcode.common.utils.DateUtils;
import com.wolfcode.common.utils.SecurityUtils;
import com.wolfcode.customer.domain.Customer;
import com.wolfcode.customer.domain.info.CustomerLinkmanInfo;
import com.wolfcode.customer.domain.vo.CustomerLinkmanVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wolfcode.customer.mapper.CustomerLinkmanMapper;
import com.wolfcode.customer.domain.CustomerLinkman;
import com.wolfcode.customer.service.ICustomerLinkmanService;

/**
 * 客户联系人Service业务层处理
 * 
 * @author wolfcode
 * @date 2022-12-09
 */
@Service
public class CustomerLinkmanServiceImpl implements ICustomerLinkmanService 
{
    @Autowired
    private CustomerLinkmanMapper customerLinkmanMapper;

    /**
     * 查询客户联系人
     * 
     * @param id 客户联系人主键
     * @return 客户联系人
     */
    @Override
    public CustomerLinkman selectCustomerLinkmanById(Long id)
    {
        return customerLinkmanMapper.selectCustomerLinkmanById(id);
    }

    /**
     * 查询客户联系人列表
     * 
     * @param customerLinkmanVo 客户联系人
     * @return 客户联系人
     */
    @Override
    public List<CustomerLinkmanInfo> selectCustomerLinkmanList(CustomerLinkmanVo customerLinkmanVo)
    {
        return customerLinkmanMapper.selectCustomerLinkmanList(customerLinkmanVo);
    }

    /**
     * 新增客户联系人
     * 
     * @param customerLinkman 客户联系人
     * @return 结果
     */
    @Override
    public int insertCustomerLinkman(CustomerLinkman customerLinkman)
    {
        Long userId = SecurityUtils.getLoginUser().getUserId();
        customerLinkman.setInputUser(userId);
        customerLinkman.setInputTime(DateUtils.getNowDate());
        return customerLinkmanMapper.insertCustomerLinkman(customerLinkman);
    }

    /**
     * 修改客户联系人
     * 
     * @param customerLinkman 客户联系人
     * @return 结果
     */
    @Override
    public int updateCustomerLinkman(CustomerLinkman customerLinkman)
    {
        return customerLinkmanMapper.updateCustomerLinkman(customerLinkman);
    }

    /**
     * 批量删除客户联系人
     * 
     * @param ids 需要删除的客户联系人主键
     * @return 结果
     */
    @Override
    public int deleteCustomerLinkmanByIds(Long[] ids)
    {
        return customerLinkmanMapper.deleteCustomerLinkmanByIds(ids);
    }

    /**
     * 删除客户联系人信息
     * 
     * @param id 客户联系人主键
     * @return 结果
     */
    @Override
    public int deleteCustomerLinkmanById(Long id)
    {
        return customerLinkmanMapper.deleteCustomerLinkmanById(id);
    }

    @Override
    public List<CustomerLinkman> search(Long customerId) {

        return customerLinkmanMapper.search(customerId);
    }
}
