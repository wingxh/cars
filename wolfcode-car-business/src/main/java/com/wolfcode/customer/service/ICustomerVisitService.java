package com.wolfcode.customer.service;

import java.util.List;

import com.wolfcode.customer.domain.Customer;
import com.wolfcode.customer.domain.CustomerVisit;
import com.wolfcode.customer.domain.info.CustomerVisitInfo;
import com.wolfcode.customer.domain.vo.CustomerVisitVo;

/**
 * 拜访信息Service接口
 * 
 * @author wolfcode
 * @date 2022-12-09
 */
public interface ICustomerVisitService 
{
    /**
     * 查询拜访信息
     * 
     * @param id 拜访信息主键
     * @return 拜访信息
     */
    public CustomerVisitInfo selectCustomerVisitById(Long id);

    /**
     * 查询拜访信息列表
     * 
     * @param customerVisit 拜访信息
     * @return 拜访信息集合
     */
    public List<CustomerVisitInfo> selectCustomerVisitList(CustomerVisit customerVisit);

    /**
     * 新增拜访信息
     * 
     * @param customerVisitVo 拜访信息
     * @return 结果
     */
    public int insertCustomerVisit(CustomerVisitVo customerVisitVo);



    public List<Customer> getCustomers();
}
