package com.wolfcode.goods.domain;

import com.wolfcode.common.core.domain.entity.SysMenu;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.wolfcode.common.annotation.Excel;
import com.wolfcode.common.core.domain.BaseEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 物品分类信息对象 goods_category
 * 
 * @author wolfcode
 * @date 2022-12-10
 */
public class GoodsCategory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 分类名称 */
    @Excel(name = "分类名称")
    private String categoryName;

    /** 描述 */
    @Excel(name = "描述")
    private String categoryDesc;

    /** id层级结构 */
    @Excel(name = "id层级结构")
    private String busiPath;

    /** 上级分类id */
    @Excel(name = "上级分类id")
    private Long parentId;

    /** 上级分类名称 */
    @Excel(name = "上级分类id")
    private String parentName;

    /** 子菜单 */
    private List<GoodsCategory> children = new ArrayList<GoodsCategory>();

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCategoryName(String categoryName) 
    {
        this.categoryName = categoryName;
    }

    public String getCategoryName() 
    {
        return categoryName;
    }
    public void setCategoryDesc(String categoryDesc) 
    {
        this.categoryDesc = categoryDesc;
    }

    public String getCategoryDesc() 
    {
        return categoryDesc;
    }
    public void setBusiPath(String busiPath) 
    {
        this.busiPath = busiPath;
    }

    public String getBusiPath() 
    {
        return busiPath;
    }
    public void setParentId(Long parentId)
    {
        this.parentId = parentId;
    }

    public Long getParentId()
    {
        return parentId;
    }

    public List<GoodsCategory> getChildren() {
        return children;
    }

    public void setChildren(List<GoodsCategory> children) {
        this.children = children;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("categoryName", getCategoryName())
            .append("categoryDesc", getCategoryDesc())
            .append("busiPath", getBusiPath())
            .append("parentId", getParentId())
            .toString();
    }
}
