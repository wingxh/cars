package com.wolfcode.common.utils;

import com.wolfcode.common.utils.file.FileUploadUtils;
import com.wolfcode.common.utils.uuid.IdUtils;
import io.minio.BucketExistsArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.errors.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class FileUpLoadUtils {
    public static String getFileAddress(MultipartFile file) throws Exception {
        // 主机地址, 即我们要存放的服务器目标地址
        String endpoint = "http://xue.cnkdl.cn:23900";
        // 账号
        String accessKey = "developer";
        // 密码
        String secretKey = "wolfcode.cn";
        // 存储空间, 即我们要存放的目录
        String bucket = "car";
        // 自己生成唯一的文件名
        String fileName = IdUtils.fastSimpleUUID() + System.currentTimeMillis();
        // 获取文件后缀
        String extension = FileUploadUtils.getExtension(file);
        InputStream inputStream = file.getInputStream();

        if (StringUtils.isNotEmpty(extension)) {
            fileName = fileName.concat(".").concat(extension);
        }

        // 使用步骤：
        // 1：创建minio客户端连接对象
        MinioClient minioClient = MinioClient.builder().endpoint(endpoint).credentials(accessKey, secretKey).build();
        // 2: 检查存储空间是否存在, 存储空间不存在上传不了
        boolean exists = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucket).build());
        if (!exists) {
            throw new RuntimeException("存储空间不存在!");
        }

        PutObjectArgs objectArgs = PutObjectArgs.builder()
                .bucket(bucket)
                .object(fileName)
                .stream(inputStream, inputStream.available(), -1)
                .build();

        // 3:上传文件
        minioClient.putObject(objectArgs);
        //4：拼接图片访问连接
        String url = endpoint + "/" + bucket + "/" + fileName;

        return url;
    }
}
