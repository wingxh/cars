package com.wolfcode.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LicensePlateUtil {
    /**
     * 校验车牌号
     * @param licensePlate
     * @return
     */
    public static boolean validate(String licensePlate){
        if (StringUtils.isEmpty(licensePlate)) {
            return false;
        }
        String pat= "^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-Z0-9]{4}[A-Z0-9挂学警港澳]{1}$";
        Pattern pattern = Pattern.compile(pat);
        Matcher match = pattern.matcher(licensePlate);
        return match.matches();
    }

    public static void main(String[] args) {
        System.out.println(validate("粤A6666"));
    }
}
