import request from "@/utils/request";

// 查询合同信息列表
export function listContract(query) {
  return request({
    url: "/business/contract/list",
    method: "get",
    params: query,
  });
}

// 查询合同信息详细
export function getContract(id) {
  return request({
    url: "/business/contract/" + id,
    method: "get",
  });
}

// 新增合同信息
export function addContract(data) {
  return request({
    url: "/business/contract",
    method: "post",
    data: data,
  });
}

// 修改合同信息
export function updateContract(data) {
  return request({
    url: "/business/contract",
    method: "put",
    data: data,
  });
}

// 删除合同信息
export function delContract(id) {
  return request({
    url: "/business/contract/" + id,
    method: "delete",
  });
}

// 合同作废
export function cancelContract(id) {
  return request({
    url: "/business/contract/cancel/" + id,
    method: "put",
  });
}

// 审核通过
export function auditSuccess(id) {
  return request({
    url: "/business/contract/auditSuccess/" + id,
    method: "put",
  });
}

// 审核不通过
export function auditFail(id) {
  return request({
    url: "/business/contract/auditFail/" + id,
    method: "put",
  });
}

// 审核不通过
export function handleSeal(id) {
  return request({
    url: "/business/contract/handleSeal/" + id,
    method: "put",
  });
}
