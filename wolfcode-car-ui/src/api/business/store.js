import request from "@/utils/request";

// 查询物品库存列表
export function listStore(query) {
  return request({
    url: "/goods/store/list",
    method: "get",
    params: query,
  });
}

// 查询物品库存详细
export function getStore(id) {
  return request({
    url: "/goods/store/" + id,
    method: "get",
  });
}

// 新增物品库存
export function addStore(data) {
  return request({
    url: "/goods/store",
    method: "post",
    data: data,
  });
}

// 修改物品库存
export function updateStore(data) {
  return request({
    url: "/goods/store",
    method: "put",
    data: data,
  });
}

// 删除物品库存
export function delStore(id) {
  return request({
    url: "/goods/store/" + id,
    method: "delete",
  });
}

// 查询物品库存列表
export function listallStore() {
  return request({
    url: "/goods/store/all",
    method: "get",
  });
}

// 根据id查询可选列表
export function SelectStoreItems(data, id) {
  return request({
    url: "/goods/store/selectList/" + id,
    method: "post",
    data: data,
  });
}
