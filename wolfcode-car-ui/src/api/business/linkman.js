import request from '@/utils/request'

// 查询客户联系人列表
export function listLinkman(query) {
  return request({
    url: '/customer/linkman/list',
    method: 'get',
    params: query
  })
}

// 查询客户联系人详细
export function getLinkman(id) {
  return request({
    url: '/customer/linkman/' + id,
    method: 'get'
  })
}

// 新增客户联系人
export function addLinkman(data) {
  return request({
    url: '/customer/linkman',
    method: 'post',
    data: data
  })
}

// 修改客户联系人
export function updateLinkman(data) {
  return request({
    url: '/customer/linkman',
    method: 'put',
    data: data
  })
}

// 删除客户联系人
export function delLinkman(id) {
  return request({
    url: '/customer/linkman/' + id,
    method: 'delete'
  })
}

// 获取客户联系人
export function searchLinkman(id) {
  return request({
    url: '/customer/linkman/search/' + id,
    method: 'get'
  })
}