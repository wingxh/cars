import request from "@/utils/request";

// 查询物品信息列表
export function listGoods(query) {
  return request({
    url: "/goods/goods/list",
    method: "get",
    params: query,
  });
}

// 查询物品信息详细
export function getGoods(id) {
  return request({
    url: "/goods/goods/" + id,
    method: "get",
  });
}

// 新增物品信息
export function addGoods(data) {
  return request({
    url: "/goods/goods",
    method: "post",
    data: data,
  });
}

// 修改物品信息
export function updateGoods(data) {
  return request({
    url: "/goods/goods",
    method: "put",
    data: data,
  });
}

// 删除物品信息
export function delGoods(id) {
  return request({
    url: "/goods/goods/" + id,
    method: "delete",
  });
}
// 获取仓库列表
export function listStore() {
  return request({
    url: "/goods/goods/stores",
    method: "get",
  });
}

// 查询物品出入库明细
export function listStockRecord(id) {
  return request({
    url: "/goods/goods/records/" + id,
    method: "get",
  });
}
