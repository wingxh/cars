import request from '@/utils/request'

// 查询出入库单据明细列表
export function listItem(query) {
  return request({
    url: '/goods/item/list',
    method: 'get',
    params: query
  })
}

// 查询出入库单据明细详细
export function getItem(id) {
  return request({
    url: '/goods/item/' + id,
    method: 'get'
  })
}

// 新增出入库单据明细
export function addItem(data) {
  return request({
    url: '/goods/item',
    method: 'post',
    data: data
  })
}

// 修改出入库单据明细
export function updateItem(data) {
  return request({
    url: '/goods/item',
    method: 'put',
    data: data
  })
}

// 删除出入库单据明细
export function delItem(id) {
  return request({
    url: '/goods/item/' + id,
    method: 'delete'
  })
}
