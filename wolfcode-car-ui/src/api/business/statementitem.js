import request from '@/utils/request'

// 新增结算单明细
export function addStatementItem(data) {
  return request({
    url: '/appointment/statement/item',
    method: 'post',
    data: data
  })
}
// 结算单明细列表
export function listStatementItem(query) {
  return request({
    url: '/appointment/statement/item/list',
    method: 'get',
    params: query
  })
}


