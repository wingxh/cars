import request from "@/utils/request";

// 查询物品分类信息列表
export function listCategory(query) {
  return request({
    url: "/goods/category/list",
    method: "get",
    params: query,
  });
}

// 查询物品分类下拉列表
export function treeselect() {
  return request({
    url: '/goods/category/treeselect',
    method: 'get',
  })
}

// 查询物品分类信息详细
export function getCategory(id) {
  return request({
    url: "/goods/category/" + id,
    method: "get",
  });
}

// 新增物品分类信息
export function addCategory(data) {
  return request({
    url: "/goods/category",
    method: "post",
    data: data,
  });
}

// 修改物品分类信息
export function updateCategory(data) {
  return request({
    url: "/goods/category",
    method: "put",
    data: data,
  });
}

// 删除物品分类信息
export function delCategory(id) {
  return request({
    url: "/goods/category/" + id,
    method: "delete",
  });
}

// 获取分类选项
export function CategorySelect() {
  return request({
    url: "/goods/category/select",
    method: "get",
  });
}
